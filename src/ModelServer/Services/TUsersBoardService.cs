using ProjetoFinal.Domain;
using Simple.Entities;

namespace ProjetoFinal.Services
{
    public partial class TUsersBoardService : EntityService<TUsersBoard>, ITUsersBoardService
    {
        public TUsersBoard Create(TUsersBoard usersBoard)
        {
            return usersBoard.Save();
        }

        public TUsersBoard FindById(int id)
        {
            return TUsersBoard.Find(x => x.Id == id);
        }
    }
}