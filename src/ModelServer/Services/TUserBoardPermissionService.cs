using ProjetoFinal.Domain;
using Simple.Entities;

namespace ProjetoFinal.Services
{
    public partial class TUserBoardPermissionService : EntityService<TUserBoardPermission>, ITUserBoardPermissionService
    {
        public TUserBoardPermission Create(TUserBoardPermission userBoardPermission)
        {
            return userBoardPermission.Save();
        }
    }
}