using System;
using System.Collections.Generic;
using System.Linq;
using ProjetoFinal.Domain;
using Simple.Entities;

namespace ProjetoFinal.Services
{
    public partial class TComentService : EntityService<TComent>, ITComentService
    {
        public TComent Create(TComent coment)
        {
            coment.CreationDate = DateTime.Now;
            coment.EditDate = DateTime.Now;
            return coment.Save();
        }

        public TComent Edit(TComent coment)
        {
            var original = FindById(coment.Id);
            original.Content = coment.Content;
            return original.Update();
        }

        public void Remove(TComent coment)
        {
            coment.Delete();
        }

        public List<TComent> ListAllcoments(int idAssignment)
        {
            var assignment = TAssignment.Service.FindById(idAssignment);
            var coments = TComent.List(x => x.Assignment.Id == assignment.Id).ToList();
            return coments;
        }

        public TComent FindById(int id)
        {
            return TComent.Find(x => x.Id == id);
        }

    }
}
