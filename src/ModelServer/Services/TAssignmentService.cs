using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Build.Utilities;
using Simple.Entities;
using ProjetoFinal.Domain;

namespace ProjetoFinal.Services
{
    public partial class TAssignmentService : EntityService<TAssignment>, ITAssignmentService
    {
        public TAssignment CreateTask(TAssignment task)
        {
            if (task.Completed == null)
                task.Completed = false;
            task.CreationDate = DateTime.Now;
            task.EditDate = DateTime.Now;
            task.Save();
            return task;
        }

        public TAssignment Edit(TAssignment task)
        {
            var original = FindById(task.Id);
            original.Completed = task.Completed == null || task.Completed == false ? false : true ;
            original.Content = task.Content;
            original.EditDate = DateTime.Now;
            original.EndDate = task.EndDate;
            original.Name = task.Name;
            original.Update();
            return original;
        }

        public void Remove(TAssignment task)
        {
            if(task.TComents.Any())
                foreach (var coment in task.TComents)
                {
                        coment.Delete();   
                }
            task.Delete();
        }

        public List<TAssignment> ListAllList()
        {
            var lisTAssignments = TAssignment.ListAll().ToList();
            return lisTAssignments;
        }

        public TAssignment FindById(int id)
        {
            return TAssignment.Find(x => x.Id == id);
        }
    
    }
}