using ProjetoFinal.Domain;
using Simple.Entities;

namespace ProjetoFinal.Services
{
    public partial class TPermissionService : EntityService<TPermission>, ITPermissionService
    {
        public TPermission Create(TPermission permission)
        {
            return permission.Save();
        }
    }
}