using System;
using System.Collections.Generic;
using System.Linq;
using ProjetoFinal.Domain;
using Simple.Entities;

namespace ProjetoFinal.Services
{
    public partial class TNotificationService : EntityService<TNotification>, ITNotificationService
    {
        public TNotification Create(TNotification notification)
        {

            notification.User = notification.User ?? TUser.FindById(SecurityContext.Do.User.Id);
            notification.CreationDate = DateTime.Now;
            return notification.Save();
        }

        public void Remove(TNotification notification)
        {
            notification.Delete();
        }

        public List<TNotification> ListAllNotifications()
        {
            var user = SecurityContext.Do.User;
            var notification = TNotification.List(x => x.User.Id == user.Id).ToList();
            return notification;
        }

        public TNotification FindById(int id)
        {
            return TNotification.Find(x => x.Id == id);
        }
    }
}