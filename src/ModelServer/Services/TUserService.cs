using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Entities;
using ProjetoFinal.Domain;
using System.Security.Cryptography;
using Microsoft.Win32;
using NHibernate.Linq;

namespace ProjetoFinal.Services
{
    public partial class TUserService : EntityService<TUser>, ITUserService
    {
        public TUser Create(TUser user)
        {
            var profile = TProfile.Service.FindById(2);
            user.Token = user.Token;
            user.ActivateAccount = false;
            user.Profile = profile;
            user.Password = HashPassword(user.PasswordString);
            user.CreationDate = user.EditDate = DateTime.Now;

            user.Save();
            return user;
        }

            public TUser Edit(TUser user)
            {
                var original = FindById(user.Id);
                original.EditDate = DateTime.Now;
                original.Email = user.Email;
                original.Name = user.Name;
                original.Website = user.Website;
                original.BirthDate = user.BirthDate;
                original.PhoneNumber = user.PhoneNumber;
                original.State = user.State;
                if (!string.IsNullOrWhiteSpace(user.Image))
                    original.Image = user.Image;
                original.Update();
                return original;
            }

            public void Remove(TUser user)
            {
                if (!user.TBoards.Any() && !user.TNotes.Any() && !user.TTags.Any() && !user.TUsersBoards.Any() && !user.TNotifications.Any())
                {
                    user.Delete();
                }
                else
                {
                    if (user.TUsersBoards.Any())
                        foreach (var userboard in user.TUsersBoards)
                        {
                            if (userboard.TUserBoardPermissions.Any())
                                foreach (var permission in userboard.TUserBoardPermissions)
                                {
                                    permission.Delete();
                                }

                            userboard.Delete();
                        }

                    if (user.TBoards.Any())
                        foreach (var board in user.TBoards)
                        {
                            if (!board.TAssignments.Any())
                                board.Delete();

                            foreach (var task in board.TAssignments)
                            {
                                if (!task.TComents.Any())
                                {
                                    task.Delete();
                                }
                                else
                                {
                                    foreach (var coment in task.TComents)
                                    {
                                        coment.Delete();
                                    }
                                    task.Delete();
                                }
                            }

                            board.Delete();
                        }

                    
                    else if (user.TNotes.Any())
                    {
                        foreach (var note in user.TNotes)
                        {
                            note.Delete();
                        }
                    }
                    else if (user.TTags.Any())
                    {
                        foreach (var tag in user.TTags)
                        {
                            tag.Delete();
                        }
                    }
                    else if (user.TNotifications.Any())
                    {
                        foreach (var notification in user.TNotifications)
                        {
                            notification.Delete();
                        }
                    }

                    user.Delete();
                }
            }

            public List<TUser> ListAllUsers()
            {
                var listUser = TUser.List(y=>y.Profile.Id != 1).OrderByDescending(x=>x.CreationDate).ToList();
                return listUser;
            }

            public List<TUser> ListAllWithActiveAccount(int boardId)
            {
                var listUsers = TUser.List(y => y.Profile.Id != 1 && y.ActivateAccount && y.TUsersBoards.All(x => x.Board.Id != boardId)).OrderByDescending(x => x.CreationDate).ToList();
                return listUsers;
            } 

            public TUser ChangeProfile(TUser user, TProfile profile)
            {
                user.Profile = profile;
                return user.Update();
            }

            public TUser ChangePassword(int id, string password)
            {
                var usuario = TUser.FindById(id);
                usuario.Password = TUser.HashPassword(password);
                return usuario.Update();
            }

            public List<TUser> Search(string query, int page, int take)
            {
                var search = TUser.List(x => x.Name.Contains(query) || x.Email.Contains(query), (page - 1) * take, take).ToList();
                return search;
            }

            public TUser FindById(int id)
            {
                return TUser.Find(x => x.Id == id);
            }

            public TUser FindByToken(string token)
            {
                return TUser.Find(x => x.Token == token);
            }

            public TUser FindByEmail(string email)
            {
                return TUser.Find(x => x.Email == email);
            }

            public TUser FindByGoogleId(string idGoogle)
            {
                return TUser.Find(x => x.GoogleId == idGoogle);
            }

            public byte[] HashPassword(string password)
            {
                return SHA384.Create().ComputeHash(Encoding.UTF8.GetBytes(password));
            }

            public TUser Authenticate(string email, string password)
            {
                var membro = FindByEmail(email);
                if (membro == null || string.IsNullOrWhiteSpace(password))
                    return null;

                bool passwordCorrect = false;
                passwordCorrect = this.HashPassword(password).SequenceEqual(membro.Password);

                if (!passwordCorrect)
                    return null;
                else
                    return membro;
            }

            public TUser AutenticateGoogleUser(string email, byte[] password)
            {
                var membro = FindByEmail(email);
                if (membro == null || password == null)
                    return null;

                bool passwordCorrect = false;
                passwordCorrect = password.SequenceEqual(membro.Password);

                if (!passwordCorrect)
                    return null;
                else
                    return membro;
            }
        }
    }
