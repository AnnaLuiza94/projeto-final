using System.Collections.Generic;
using System.Linq;
using ProjetoFinal.Domain;
using Simple.Entities;

namespace ProjetoFinal.Services
{
    public partial class TTagService : EntityService<TTag>, ITTagService
    {
        public TTag Create(TTag tag)
        {
            var user = SecurityContext.Do.User;
            tag.User = user;
            return tag.Save();
        }

        public List<TTag> ListAlltags()
        {
            var user = SecurityContext.Do.User;
            var tags = TTag.List(x => x.User.Id == user.Id || x.FromSystem).OrderByDescending(x => x.FromSystem).ToList();
            return tags;
        }

        public TTag Edit(TTag tag)
        {
            var original = FindById(tag.Id);
            original.Name = tag.Name;
            original.IsArchivied = tag.IsArchivied;
            return original.Update();
        }

        public void Remove(TTag tag)
        {
            if (tag.TBoards.Any())
            {
                foreach (var board in tag.TBoards)
                {
                    board.Delete();
                }
            }
            tag.Delete();
        }
        

        public TTag FindById(int id)
        {
            return TTag.Find(x => x.Id == id);
        }
    }
}