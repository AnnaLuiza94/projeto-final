using System.Collections.Generic;
using System.Linq;
using ProjetoFinal.Domain;
using Simple.Entities;

namespace ProjetoFinal.Services
{
    public partial class TProfileService : EntityService<TProfile>, ITProfileService
    {
        public TProfile Create(TProfile profile)
        {
            profile.Save();
            return profile;
        }

        public void Remove(TProfile profile)
        {
            if (profile.TUsers.Any())
            {
                foreach (var user in profile.TUsers)
                {
                    user.Delete();
                }
            }
            profile.Delete();
        }

        public List<TProfile> ListAllProfiles()
        {
            return TProfile.ListAll(x => x.OrderByDesc(y => y.Id)).ToList();
        }

        public TProfile FindById(int id)
        {
            return TProfile.Find(x => x.Id == id);
        }
    }
}