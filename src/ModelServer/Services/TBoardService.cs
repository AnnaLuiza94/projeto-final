using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Internal;
using NHibernate.Type;
using NHibernate.Util;
using ProjetoFinal.Domain;
using ProjetoFinal.Helpers;
using Simple;
using Simple.Entities;

namespace ProjetoFinal.Services
{
    public partial class TBoardService : EntityService<TBoard>, ITBoardService
    {
        public TBoard Create(TBoard board)
        {
            if (board != null)
            {
                board.Done = board.Done ?? (board.Done = false);
                board.Favorite = board.Favorite ?? (board.Favorite = false);
                board.Important = board.Important ?? (board.Important = false);
                board.IsArchived = board.IsArchived ?? (board.IsArchived = false);
                board.IsShared = false;
                board.CreationDate = DateTime.Now; ;
                board.Save();
            }
            return board;

        }

        public TBoard Edit(TBoard board)
        {
            var original = FindById(board.Id);

            original.Name = board.Name;
            original.EditDate = DateTime.Now;
            original.Important = board.Important != null ? board.Important : false;
            original.Favorite = board.Favorite != null ? board.Favorite : false;
            original.Done = board.Done != null ? board.Done : false;
            original.IsArchived = board.IsArchived != null ? board.IsArchived : false;
            original.Update();
            return original;
        }

        public void Remove(TBoard board)
        {
            if (board.TAssignments.Any())
            {
                foreach (var assigment in board.TAssignments)
                {
                    if (assigment.TComents.Any())
                    {
                        foreach (var coment in assigment.TComents)
                        {
                            coment.Delete();
                        }
                    }

                    assigment.Delete();
                }
            }
            if (board.TUsersBoards.Any())
            {
                foreach (var usersBoard in board.TUsersBoards)
                {
                    if (usersBoard.TUserBoardPermissions.Any())
                    {
                        foreach (var userPermission in usersBoard.TUserBoardPermissions)
                        {
                            userPermission.Delete();
                        }
                    }
                    usersBoard.Delete();
                }
            }


            board.Delete();
        }

        public void AddUsersBoard(int idBoard, List<string> emails)
        {
            var board = TBoard.FindById(idBoard);
            var userboard = new TUsersBoard();
            foreach (var email in emails)
            {
                var user = TUser.FindByEmail(email);
                userboard.User = user;
                userboard.Board = board;
                board.TUsersBoards.Add(userboard);
            }
        }

        public TBoard ClearAllTasks(TBoard board)
        {
            if (board.TAssignments.Any())
            {
                foreach (var assignment in board.TAssignments)
                {
                    if (assignment.TComents.Any())
                    {
                        foreach (var comment in assignment.TComents)
                        {
                            comment.Remove();
                        }
                    }
                    assignment.Update();
                    assignment.Remove();
                }
                board.Update();
            }
            return board;
        }

        public List<TBoard> ListAllByFilters()
        {
            var user = SecurityContext.Do.User;
            var listTBoards = TBoard.List(x => x.IsArchived == false && (x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id))).ToList();
            return listTBoards;
        }

        public List<TBoard> ListAllIncludeArchiveds()
        {
            var user = SecurityContext.Do.User;
            var listTBoards = TBoard.ListAll().ToList();//TBoard.List(x => x.User.Id == user.Id).ToList();
            return listTBoards;
        }

        public List<TBoard> ListAllBoards()
        {
            var user = SecurityContext.Do.User;
            var listBoards = TBoard.List(x => x.IsArchived == false && (x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id))).ToList();
            return listBoards;
        }


        public List<TBoard> ListAllTagBoards()
        {
            var user = SecurityContext.Do.User;
            var boards = TBoard.List(x => x.Tag.TBoards.Any(y => y.CreatedBy.Id == user.Id)).ToList();
            return boards;
        }

        public List<TBoard> ListBoards(int page, int take)
        {
            var user = SecurityContext.Do.User;
            var boards = TBoard.ListAll(o => o.OrderByDesc(x => x.CreationDate), (page - 1) * take, take).Where(x => x.IsArchived == false && (x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id))).ToList();
            return boards;
        }

        public List<TBoard> ListBoardsWithTaskIncompleted(int page, int take)
        {
            var user = SecurityContext.Do.User;
            var boards = TBoard.ListAll(o => o.OrderByDesc(x => x.CreationDate), (page - 1) * take, take);
            return boards.Where(x => x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id) && x.IsArchived != true && x.TAssignments.Any(y => y.Completed == false)).ToList();

        }

        public List<TBoard> ListBoardsByFilter(/*int page, int take,*/ bool isArchived, bool isFavorite, bool isImportant, bool isDone, bool isInbox)
        {
            var user = SecurityContext.Do.User;
            var boards = new List<TBoard>();
            if (isArchived) //listar quadros arquivados
                //boards = TBoard.ListAll(o => o.OrderByDesc(x => x.CreationDate), (page - 1) * take, take).Where(x => x.CreatedBy.Id == user.Id && x.IsArchived == true).ToList();
                boards = TBoard.List(x => x.CreatedBy.Id == user.Id && x.IsArchived == true).ToList();
            else if (isFavorite) //listar favoritos
            {
                boards =
                    TBoard.List(
                            x =>
                                x.CreatedBy.Id == user.Id ||
                                x.TUsersBoards.Any(y => y.User.Id == user.Id) && x.IsArchived == false).ToList();
                return boards.Where(x => x.Favorite == true).ToList();
            }
            else if (isImportant) //listar importantes 
            {
                boards = TBoard.List(x => x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id)).ToList();
                return boards.Where(x => x.Important == true && x.IsArchived == false).ToList();
            }
            else if (isDone) //listar quadros concluidos
            {
                boards = TBoard.List(x => x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id)).ToList();
                return boards.Where(x => x.TAssignments.All(o=>o.Completed == true) && x.IsArchived == false).ToList();
            }
            else if (isInbox) //listar quadros que contem pelo menos um tarefa nao concluida
            {
                boards = TBoard.List(x => x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id)).ToList();
                return boards.Where(x => x.IsArchived == false && x.TAssignments.Any(o => o.Completed == false)).ToList();
            }
            return boards;
        }

        public int CountTotalBoardsByFilters(bool isArchived, bool isFavorite, bool isImportant, bool isDone, bool isInbox)
        {
            var user = SecurityContext.Do.User;
            if (isArchived)
                return TBoard.Count(x => x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id) && x.IsArchived == true);
            if (isFavorite)
                return TBoard.Count(x => x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id) && x.IsArchived != true && x.Favorite == true);
            if (isImportant)
                return TBoard.Count(x => x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id) && x.IsArchived != true && x.Important == true);
            if (isDone)
                return TBoard.Count(x => x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id) && x.IsArchived != true && x.Done == true);
            if (isInbox)
                TBoard.Count(x => x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id) && x.IsArchived != true && x.TAssignments.Any(y => y.Completed == false));

            return TBoard.Count(x => x.CreatedBy.Id == user.Id || x.TUsersBoards.Any(y => y.User.Id == user.Id) && x.IsArchived != true);
        }

        public TBoard FindById(int id)
        {
            return TBoard.Find(x => x.Id == id);
        }


    }
}
