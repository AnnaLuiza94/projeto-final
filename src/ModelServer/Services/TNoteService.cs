using System;
using System.Collections.Generic;
using System.Linq;
using ProjetoFinal.Domain;
using Simple.Entities;

namespace ProjetoFinal.Services
{
    public partial class TNoteService : EntityService<TNote>, ITNoteService
    {
        public TNote Create(TNote note)
        {
                note.CreationDate = DateTime.Now;
                return note.Save();
        }

        public TNote Edit(TNote note)
        {
            var original = FindById(note.Id);
            original.Name = note.Name;
            original.Content = note.Content;
            return original.Update();
        }

        public void Remove(TNote note)
        {
            note.Delete();
        }

        public List<TNote> ListAllNotes()
        {
            var user = SecurityContext.Do.User;
            var notes = TNote.List(x => x.User.Id == user.Id).ToList();
            return notes;
        }

        public TNote FindById(int id)
        {
            return TNote.Find(x => x.Id == id);
        }

    }
}