﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjetoFinal.Domain
{

    public partial class TBoard
    {

        public virtual int favoriteElements(int id)
        {
            var boards = List(x => x.CreatedBy.Id == id || x.TUsersBoards.Any(y => y.User.Id == id)).ToList();
            if (boards != null)
                return boards.Count(x => x.Favorite == true && x.IsArchived == false);

            return 0;

        }

        public virtual int importantElements(int id)
        {
            var boards = List(x => x.CreatedBy.Id == id || x.TUsersBoards.Any(y => y.User.Id == id)).ToList();
            if (boards != null)
                return boards.Count(x => x.Important == true && x.IsArchived == false);

            return 0;
        }

        public virtual int completedTaskElements(int id)
        {
            var boards = List(x => x.CreatedBy.Id == id || x.TUsersBoards.Any(y => y.User.Id == id)).ToList();
            if (boards != null)
                return boards.Count(x => x.IsArchived == false && x.TAssignments.Any(y => y.Completed == true));

            return 0;
        }

        public virtual int notCompletedTaskBoardElements(int id)
        {
            var boards = List(x => x.IsArchived == false && (x.CreatedBy.Id == id || x.TUsersBoards.Any(y => y.User.Id == id))).ToList();
            if (boards != null)
                return boards.Count(x => x.IsArchived == false && x.TAssignments.Any(y => y.Completed == false));

            return 0;

        }

        public virtual int archivedBoardElements(int id)
        {

            var boards = List(x => x.CreatedBy.Id == id).ToList();
            if (boards.Any())
                return boards.Count(x => x.IsArchived == true);

            return 0;

        }

        public virtual int doneBoardOrDoneAlltasksBoard(int id)
        {
            var boards = List(x => x.CreatedBy.Id == id || x.TUsersBoards.Any(y => y.User.Id == id) && x.IsArchived == false).ToList();
            if (boards != null)
                return boards.Count((x => x.Done == true && x.TAssignments.All(y => y.Completed == true) && x.TAssignments.Count > 0));

            return 0;
        }

        public virtual void addBoardNotifications(int id)
        {
            var boards = List(x => x.CreatedBy.Id == id || x.TUsersBoards.Any(y => y.User.Id == id) && x.IsArchived == false).ToList();
            var boardsWithTasksFalse = notCompletedTaskBoardElements(id); //pegar todos os quadros que possuem tarefas falsas
            var user = TUser.FindById(id);
            foreach (var board in boards)
            {

                var taskBoardFalseElements = board.TAssignments.Count(x => x.Completed == false);  //Pegar todas as tarefas incompletas do quadro do foreach
                if (taskBoardFalseElements != 0)
                {
                    TNotification notification = new TNotification();
                    notification.Title = "Tarefa(s) não-concluída(s) no quadro: " + board.Name;
                    notification.Details = taskBoardFalseElements + "tarefa(s) não concluída(s) em: " + board.Name;
                    TNotification.Service.Create(notification);
                }
                else
                {
                    TNotification notification = new TNotification();
                    notification.Title = "Parabéns! Você não possui tarefas pendentes";
                    notification.Details = "Parabéns, você não possui tarefa(s) não concluída(s) em: " + board.Name;
                    TNotification.Service.Create(notification);
                }
            }
        }
    }
}
