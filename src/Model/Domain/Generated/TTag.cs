using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TTag : Entity<TTag, ITTagService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Name { get; set; } 
        public virtual Boolean? IsArchivied { get; set; } 
        public virtual Boolean FromSystem { get; set; } 
        public virtual String Color { get; set; } 

        public virtual TUser User { get; set; } 

        public virtual ICollection<TBoard> TBoards { get; set; } 

        #region ' Generated Helpers '
        static TTag()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TTag obj1, TTag obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TTag obj1, TTag obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TTag() 
        {
            this.TBoards = new HashSet<TBoard>();
            Initialize();
        }
        
        public override TTag Clone()
        {
            var cloned = base.Clone();
            cloned.TBoards = null;
            return cloned;
        }

        public TTag(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}