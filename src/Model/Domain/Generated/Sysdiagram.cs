using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class Sysdiagram : Entity<Sysdiagram, ISysdiagramService>
    {
        public virtual Int32 DiagramId { get; set; } 

        public virtual String Name { get; set; } 
        public virtual Int32 PrincipalId { get; set; } 
        public virtual Int32? Version { get; set; } 
        public virtual Byte[] Definition { get; set; } 



        #region ' Generated Helpers '
        static Sysdiagram()
        {
            Identifiers
                .Add(x => x.DiagramId)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(Sysdiagram obj1, Sysdiagram obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(Sysdiagram obj1, Sysdiagram obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public Sysdiagram() 
        {
            Initialize();
        }
        
        public override Sysdiagram Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public Sysdiagram(Int32 DiagramId) : this()
        {  
            this.DiagramId = DiagramId;
        }
     
        #endregion

    }
}