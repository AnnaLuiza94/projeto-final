using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    public partial class TUserBoardPermission
    {
        public virtual TUserBoardPermission Create() 
        {
			return Service.Create(this);
		}

    }
}