using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Domain
{
    public partial class TProfile
    {
        public virtual TProfile Create() 
        {
			return Service.Create(this);
		}

        public virtual void Remove() 
        {
			Service.Remove(this);
		}

        public static List<TProfile> ListAllProfiles() 
        {
			return Service.ListAllProfiles();
		}

        public static TProfile FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

    }
}