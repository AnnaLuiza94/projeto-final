using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Domain
{
    public partial class TNote
    {
        public virtual TNote Create() 
        {
			return Service.Create(this);
		}

        public virtual TNote Edit() 
        {
			return Service.Edit(this);
		}

        public virtual void Remove() 
        {
			Service.Remove(this);
		}

        public static List<TNote> ListAllNotes() 
        {
			return Service.ListAllNotes();
		}

        public static TNote FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

    }
}