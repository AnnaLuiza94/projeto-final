using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TUserPermission : Entity<TUserPermission, ITUserPermissionService>
    {
        public virtual Int32 Id { get; set; } 


        public virtual TPermission Permission { get; set; } 
        public virtual TUser User { get; set; } 


        #region ' Generated Helpers '
        static TUserPermission()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TUserPermission obj1, TUserPermission obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TUserPermission obj1, TUserPermission obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TUserPermission() 
        {
            Initialize();
        }
        
        public override TUserPermission Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TUserPermission(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}