using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Domain
{
    public partial class TAssignment
    {
        public virtual TAssignment CreateTask() 
        {
			return Service.CreateTask(this);
		}

        public virtual TAssignment Edit() 
        {
			return Service.Edit(this);
		}

        public virtual void Remove() 
        {
			Service.Remove(this);
		}

        public static List<TAssignment> ListAllList() 
        {
			return Service.ListAllList();
		}

        public static TAssignment FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

    }
}