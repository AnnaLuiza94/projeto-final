using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Domain
{
    public partial class TUser
    {
        public virtual TUser Create() 
        {
			return Service.Create(this);
		}

        public virtual TUser Edit() 
        {
			return Service.Edit(this);
		}

        public virtual void Remove() 
        {
			Service.Remove(this);
		}

        public static List<TUser> ListAllUsers() 
        {
			return Service.ListAllUsers();
		}

        public static List<TUser> ListAllWithActiveAccount(Int32 boardId) 
        {
			return Service.ListAllWithActiveAccount(boardId);
		}

        public virtual TUser ChangeProfile(TProfile profile) 
        {
			return Service.ChangeProfile(this, profile);
		}

        public static TUser ChangePassword(Int32 id, String password) 
        {
			return Service.ChangePassword(id, password);
		}

        public static List<TUser> Search(String query, Int32 page, Int32 take) 
        {
			return Service.Search(query, page, take);
		}

        public static TUser FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

        public static TUser FindByToken(String token) 
        {
			return Service.FindByToken(token);
		}

        public static TUser FindByEmail(String email) 
        {
			return Service.FindByEmail(email);
		}

        public static TUser FindByGoogleId(String idGoogle) 
        {
			return Service.FindByGoogleId(idGoogle);
		}

        public static Byte[] HashPassword(String password) 
        {
			return Service.HashPassword(password);
		}

        public static TUser Authenticate(String email, String password) 
        {
			return Service.Authenticate(email, password);
		}

        public static TUser AutenticateGoogleUser(String email, Byte[] password) 
        {
			return Service.AutenticateGoogleUser(email, password);
		}

    }
}