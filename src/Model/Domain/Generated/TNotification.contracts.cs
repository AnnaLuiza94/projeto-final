using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Domain
{
    public partial class TNotification
    {
        public virtual TNotification Create() 
        {
			return Service.Create(this);
		}

        public virtual void Remove() 
        {
			Service.Remove(this);
		}

        public static List<TNotification> ListAllNotifications() 
        {
			return Service.ListAllNotifications();
		}

        public static TNotification FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

    }
}