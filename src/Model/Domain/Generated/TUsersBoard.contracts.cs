using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;

namespace ProjetoFinal.Domain
{
    public partial class TUsersBoard
    {
        public virtual TUsersBoard Create() 
        {
			return Service.Create(this);
		}

        public static TUsersBoard FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

    }
}