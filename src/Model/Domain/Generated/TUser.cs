using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TUser : Entity<TUser, ITUserService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Name { get; set; } 
        public virtual String Email { get; set; } 
        public virtual Byte[] Password { get; set; } 
        public virtual DateTime CreationDate { get; set; } 
        public virtual DateTime EditDate { get; set; } 
        public virtual String State { get; set; } 
        public virtual String PhoneNumber { get; set; } 
        public virtual DateTime? BirthDate { get; set; } 
        public virtual Boolean ActivateAccount { get; set; } 
        public virtual String Image { get; set; } 
        public virtual String GoogleId { get; set; } 
        public virtual String Website { get; set; } 
        public virtual String Token { get; set; } 

        public virtual TProfile Profile { get; set; } 

        public virtual ICollection<TBoard> TBoards { get; set; } 
        public virtual ICollection<TComent> TComents { get; set; } 
        public virtual ICollection<TNote> TNotes { get; set; } 
        public virtual ICollection<TNotification> TNotifications { get; set; } 
        public virtual ICollection<TTag> TTags { get; set; } 
        public virtual ICollection<TUsersBoard> TUsersBoards { get; set; } 

        #region ' Generated Helpers '
        static TUser()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TUser obj1, TUser obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TUser obj1, TUser obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TUser() 
        {
            this.TBoards = new HashSet<TBoard>();
            this.TComents = new HashSet<TComent>();
            this.TNotes = new HashSet<TNote>();
            this.TNotifications = new HashSet<TNotification>();
            this.TTags = new HashSet<TTag>();
            this.TUsersBoards = new HashSet<TUsersBoard>();
            Initialize();
        }
        
        public override TUser Clone()
        {
            var cloned = base.Clone();
            cloned.TBoards = null;
            cloned.TComents = null;
            cloned.TNotes = null;
            cloned.TNotifications = null;
            cloned.TTags = null;
            cloned.TUsersBoards = null;
            return cloned;
        }

        public TUser(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}