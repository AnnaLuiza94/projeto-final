using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TTask : Entity<TTask, ITTaskService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual Boolean? Completed { get; set; } 
        public virtual String Content { get; set; } 
        public virtual String Name { get; set; } 



        #region ' Generated Helpers '
        static TTask()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TTask obj1, TTask obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TTask obj1, TTask obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TTask() 
        {
            Initialize();
        }
        
        public override TTask Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TTask(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}