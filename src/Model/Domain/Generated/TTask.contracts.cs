using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System.Collections.Generic;
using System;

namespace ProjetoFinal.Domain
{
    public partial class TTask
    {
        public virtual TTask CreateTask() 
        {
			return Service.CreateTask(this);
		}

        public virtual TTask Create() 
        {
			return Service.Create(this);
		}

        public virtual TTask Edit() 
        {
			return Service.Edit(this);
		}

        public static List<TTask> ListAllList() 
        {
			return Service.ListAllList();
		}

        public static TTask FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

    }
}