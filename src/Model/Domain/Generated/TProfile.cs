using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TProfile : Entity<TProfile, ITProfileService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Name { get; set; } 
        public virtual String Description { get; set; } 


        public virtual ICollection<TUser> TUsers { get; set; } 

        #region ' Generated Helpers '
        static TProfile()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TProfile obj1, TProfile obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TProfile obj1, TProfile obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TProfile() 
        {
            this.TUsers = new HashSet<TUser>();
            Initialize();
        }
        
        public override TProfile Clone()
        {
            var cloned = base.Clone();
            cloned.TUsers = null;
            return cloned;
        }

        public TProfile(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}