using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Domain
{
    public partial class TComent
    {
        public virtual TComent Create() 
        {
			return Service.Create(this);
		}

        public virtual TComent Edit() 
        {
			return Service.Edit(this);
		}

        public virtual void Remove() 
        {
			Service.Remove(this);
		}

        public static List<TComent> ListAllcoments(Int32 idAssignment) 
        {
			return Service.ListAllcoments(idAssignment);
		}

        public static TComent FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

    }
}