using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TNote : Entity<TNote, ITNoteService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Name { get; set; } 
        public virtual String Content { get; set; } 
        public virtual DateTime CreationDate { get; set; } 

        public virtual TUser User { get; set; } 


        #region ' Generated Helpers '
        static TNote()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TNote obj1, TNote obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TNote obj1, TNote obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TNote() 
        {
            Initialize();
        }
        
        public override TNote Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TNote(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}