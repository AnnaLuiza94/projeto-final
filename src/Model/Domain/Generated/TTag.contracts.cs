using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System.Collections.Generic;
using System;

namespace ProjetoFinal.Domain
{
    public partial class TTag
    {
        public virtual TTag Create() 
        {
			return Service.Create(this);
		}

        public static List<TTag> ListAlltags() 
        {
			return Service.ListAlltags();
		}

        public virtual TTag Edit() 
        {
			return Service.Edit(this);
		}

        public virtual void Remove() 
        {
			Service.Remove(this);
		}

        public static TTag FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

    }
}