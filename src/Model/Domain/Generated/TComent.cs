using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TComent : Entity<TComent, ITComentService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Content { get; set; } 
        public virtual DateTime CreationDate { get; set; } 
        public virtual DateTime EditDate { get; set; } 

        public virtual TAssignment Assignment { get; set; } 
        public virtual TUser CreatedBy { get; set; } 


        #region ' Generated Helpers '
        static TComent()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TComent obj1, TComent obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TComent obj1, TComent obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TComent() 
        {
            Initialize();
        }
        
        public override TComent Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TComent(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}