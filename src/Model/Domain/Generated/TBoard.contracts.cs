using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Domain
{
    public partial class TBoard
    {
        public virtual TBoard Create() 
        {
			return Service.Create(this);
		}

        public virtual TBoard Edit() 
        {
			return Service.Edit(this);
		}

        public virtual void Remove() 
        {
			Service.Remove(this);
		}

        public static void AddUsersBoard(Int32 idBoard, List<String> emails) 
        {
			Service.AddUsersBoard(idBoard, emails);
		}

        public virtual TBoard ClearAllTasks() 
        {
			return Service.ClearAllTasks(this);
		}

        public static List<TBoard> ListAllByFilters() 
        {
			return Service.ListAllByFilters();
		}

        public static List<TBoard> ListAllIncludeArchiveds() 
        {
			return Service.ListAllIncludeArchiveds();
		}

        public static List<TBoard> ListAllBoards() 
        {
			return Service.ListAllBoards();
		}

        public static List<TBoard> ListAllTagBoards() 
        {
			return Service.ListAllTagBoards();
		}

        public static List<TBoard> ListBoards(Int32 page, Int32 take) 
        {
			return Service.ListBoards(page, take);
		}

        public static List<TBoard> ListBoardsWithTaskIncompleted(Int32 page, Int32 take) 
        {
			return Service.ListBoardsWithTaskIncompleted(page, take);
		}

        public static List<TBoard> ListBoardsByFilter(Boolean isArchived, Boolean isFavorite, Boolean isImportant, Boolean isDone, Boolean isInbox) 
        {
			return Service.ListBoardsByFilter(isArchived, isFavorite, isImportant, isDone, isInbox);
		}

        public static Int32 CountTotalBoardsByFilters(Boolean isArchived, Boolean isFavorite, Boolean isImportant, Boolean isDone, Boolean isInbox) 
        {
			return Service.CountTotalBoardsByFilters(isArchived, isFavorite, isImportant, isDone, isInbox);
		}

        public static TBoard FindById(Int32 id) 
        {
			return Service.FindById(id);
		}

    }
}