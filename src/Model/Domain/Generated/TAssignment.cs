using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TAssignment : Entity<TAssignment, ITAssignmentService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual Boolean? Completed { get; set; } 
        public virtual String Content { get; set; } 
        public virtual String Name { get; set; } 
        public virtual DateTime? CreationDate { get; set; } 
        public virtual DateTime? EditDate { get; set; } 
        public virtual DateTime? EndDate { get; set; } 
        public virtual DateTime? FinishedIn { get; set; } 

        public virtual TBoard Board { get; set; } 

        public virtual ICollection<TComent> TComents { get; set; } 

        #region ' Generated Helpers '
        static TAssignment()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TAssignment obj1, TAssignment obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TAssignment obj1, TAssignment obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TAssignment() 
        {
            this.TComents = new HashSet<TComent>();
            Initialize();
        }
        
        public override TAssignment Clone()
        {
            var cloned = base.Clone();
            cloned.TComents = null;
            return cloned;
        }

        public TAssignment(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}