using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TNotification : Entity<TNotification, ITNotificationService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Title { get; set; } 
        public virtual String Details { get; set; } 
        public virtual DateTime? CreationDate { get; set; } 

        public virtual TUser User { get; set; } 


        #region ' Generated Helpers '
        static TNotification()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TNotification obj1, TNotification obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TNotification obj1, TNotification obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TNotification() 
        {
            Initialize();
        }
        
        public override TNotification Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TNotification(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}