using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TPermission : Entity<TPermission, ITPermissionService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Name { get; set; } 
        public virtual Permission? EnumPermission { get; set; } 


        public virtual ICollection<TUserBoardPermission> TUserBoardPermissions { get; set; } 

        #region ' Generated Helpers '
        static TPermission()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TPermission obj1, TPermission obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TPermission obj1, TPermission obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TPermission() 
        {
            this.TUserBoardPermissions = new HashSet<TUserBoardPermission>();
            Initialize();
        }
        
        public override TPermission Clone()
        {
            var cloned = base.Clone();
            cloned.TUserBoardPermissions = null;
            return cloned;
        }

        public TPermission(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}