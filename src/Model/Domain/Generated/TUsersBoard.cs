using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TUsersBoard : Entity<TUsersBoard, ITUsersBoardService>
    {
        public virtual Int32 Id { get; set; } 


        public virtual TBoard Board { get; set; } 
        public virtual TUser User { get; set; } 

        public virtual ICollection<TUserBoardPermission> TUserBoardPermissions { get; set; } 

        #region ' Generated Helpers '
        static TUsersBoard()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TUsersBoard obj1, TUsersBoard obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TUsersBoard obj1, TUsersBoard obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TUsersBoard() 
        {
            this.TUserBoardPermissions = new HashSet<TUserBoardPermission>();
            Initialize();
        }
        
        public override TUsersBoard Clone()
        {
            var cloned = base.Clone();
            cloned.TUserBoardPermissions = null;
            return cloned;
        }

        public TUsersBoard(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}