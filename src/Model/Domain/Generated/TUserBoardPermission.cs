using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TUserBoardPermission : Entity<TUserBoardPermission, ITUserBoardPermissionService>
    {
        public virtual Int32 Id { get; set; } 


        public virtual TUsersBoard UserBoard { get; set; } 
        public virtual TPermission Permission { get; set; } 


        #region ' Generated Helpers '
        static TUserBoardPermission()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TUserBoardPermission obj1, TUserBoardPermission obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TUserBoardPermission obj1, TUserBoardPermission obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TUserBoardPermission() 
        {
            Initialize();
        }
        
        public override TUserBoardPermission Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TUserBoardPermission(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}