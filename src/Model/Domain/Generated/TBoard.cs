using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TBoard : Entity<TBoard, ITBoardService>
    {
        public virtual Int32 Id { get; set; } 

        public virtual String Name { get; set; } 
        public virtual DateTime CreationDate { get; set; } 
        public virtual Boolean? Favorite { get; set; } 
        public virtual Boolean? Important { get; set; } 
        public virtual Boolean? Done { get; set; } 
        public virtual Boolean? IsArchived { get; set; } 
        public virtual Boolean IsShared { get; set; } 
        public virtual DateTime? FinishedIn { get; set; } 
        public virtual DateTime? EditDate { get; set; } 

        public virtual TTag Tag { get; set; } 
        public virtual TUser CreatedBy { get; set; } 

        public virtual ICollection<TAssignment> TAssignments { get; set; } 
        public virtual ICollection<TUsersBoard> TUsersBoards { get; set; } 

        #region ' Generated Helpers '
        static TBoard()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TBoard obj1, TBoard obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TBoard obj1, TBoard obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TBoard() 
        {
            this.TAssignments = new HashSet<TAssignment>();
            this.TUsersBoards = new HashSet<TUsersBoard>();
            Initialize();
        }
        
        public override TBoard Clone()
        {
            var cloned = base.Clone();
            cloned.TAssignments = null;
            cloned.TUsersBoards = null;
            return cloned;
        }

        public TBoard(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}