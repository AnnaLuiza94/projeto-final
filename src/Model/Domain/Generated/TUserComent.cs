using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Reflection;
using Simple.Entities;
using ProjetoFinal.Services;

namespace ProjetoFinal.Domain
{
    [Serializable]
    public partial class TUserComent : Entity<TUserComent, ITUserComentService>
    {
        public virtual Int32 Id { get; set; } 


        public virtual TComent Coment { get; set; } 
        public virtual TUser User { get; set; } 


        #region ' Generated Helpers '
        static TUserComent()
        {
            Identifiers
                .Add(x => x.Id)
;
        }
        
        partial void Initialize();
        
        public static bool operator ==(TUserComent obj1, TUserComent obj2)
        {
            return object.Equals(obj1, obj2);
        }

        public static bool operator !=(TUserComent obj1, TUserComent obj2)
        {
            return !(obj1 == obj2);
        }
        
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        
        public TUserComent() 
        {
            Initialize();
        }
        
        public override TUserComent Clone()
        {
            var cloned = base.Clone();
            return cloned;
        }

        public TUserComent(Int32 Id) : this()
        {  
            this.Id = Id;
        }
     
        #endregion

    }
}