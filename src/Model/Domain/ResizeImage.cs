﻿namespace ProjetoFinal.Domain
{
    public class ResizeImage
    {
       public string Height { get; set; }
        public string Width { get; set; }

        public ResizeImage(string height, string width)
        {
            Height = height;
            Width = width;
        }
    }
}
