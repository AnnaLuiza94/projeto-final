﻿using System;
using Simple.Validation;

namespace ProjetoFinal.Domain
{
    public class JsonElement
    {
        public String id { get; set; }
        public String text { get; set; }
        public String email { get; set; }

        public JsonElement()
        {
            this.id = "";
            this.text = "";
            this.email = "";
        }

        public JsonElement(TUser user)
        {
            this.id = user.Id.ToString();
            this.text = user.Name;
            this.email = user.Email;
        }

        public JsonElement(ValidationItem validationItem)
        {
            this.id = validationItem.PropertyName;
            this.text = validationItem.Message;
            this.email = validationItem.Message;
        }
    }

}
