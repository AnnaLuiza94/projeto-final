﻿using System.ComponentModel;

namespace ProjetoFinal.Domain
{
    public enum Permission
    {
        [Description("Administrador do quadro")]
        BoardAdministrator = 1,
        [Description("Colaborador")]
        BoardColaborator
    }
}
