﻿using System.Collections.Generic;
using System.Linq;

namespace ProjetoFinal.Domain
{
    public class NotificationBoardsPage
    {
        public virtual List<TBoard> Boards { get; set; }
        public virtual List<TNotification> Notifications { get; set; }

        public List<TNotification> ShowNotifications()
        {
            var user = SecurityContext.Do.User;
            var notifications = TNotification.List(x => x.User.Id == user.Id).ToList();
            return notifications;
        }
    }
}
