﻿using System.Collections.Generic;

namespace ProjetoFinal.Domain
{
    public class BoardPage : Page
    {
        public virtual List<TBoard> Boards { get; set; }

        public BoardPage(List<TBoard> boards, int currentPage, int itemsPerPage, int totalItems)
        {
            Boards = boards;
            CurrentPage = currentPage;
            ItemsPerPage = itemsPerPage;
            TotalItems = totalItems;
        }
    }
}
