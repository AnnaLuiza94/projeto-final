﻿using System.Linq;

namespace ProjetoFinal.Domain
{
    public partial class TUsersBoard
    {
        public virtual int favoriteElements(int id)
        {
            var userBoards = List(x => x.Id == id && x.Board.IsArchived != true).ToList();
            if (userBoards != null)
                return userBoards.Count(x => x.Board.Favorite == true);

            return 0;
        }

        public virtual int importantElements(int id)
        {
            var userBoards = List(x => x.User.Id == id && x.Board.IsArchived != true).ToList();
            if (userBoards != null)
                return userBoards.Count(x => x.Board.Important == true);

            return 0;
        }

        public virtual int completedTaskElements(int id)
        {
            var userBoards = List(x => x.User.Id == id && x.Board.IsArchived != true).ToList();
            if (userBoards != null)
                return userBoards.Count(x => x.Board.TAssignments.Any(y => y.Completed == true));

            return 0;
        }

        public virtual int notCompletedTaskBoardElements(int id)
        {
            var userBoards = List(x => x.User.Id == id && x.Board.IsArchived != true).ToList();
            if (userBoards != null)
                return userBoards.Count(x => x.Board.TAssignments.Any(y => y.Completed == false));

            return 0;

        }

        public virtual int archivedBoardElements(int id)
        {

            var userBoards = List(x => x.User.Id == id).ToList();
            if (userBoards.Any())
                return userBoards.Count(x => x.Board.IsArchived == true);

            return 0;

        }

        public virtual int doneBoardOrDoneAlltasksBoard(int id)
        {
            var userBoards = List(x => x.User.Id == id && x.Board.IsArchived != true).ToList();
            if (userBoards != null)
                return userBoards.Count((x => x.Board.Done == true || x.Board.TAssignments.All(y => y.Completed == true) && x.Board.TAssignments.Count > 0));

            return 0;
        }

        public virtual void addBoardNotifications(int id)
        {

            var userBoards = List(x => x.User.Id == id && x.Board.IsArchived != true).ToList();
            var boardsWithTasksFalse = notCompletedTaskBoardElements(id); //pegar todos os quadros que possuem tarefas falsas
            var user = TUser.FindById(id);
            foreach (var item in userBoards)
            {

                var taskBoardFalseElements = item.Board.TAssignments.Count(x => x.Completed == false);  //Pegar todas as tarefas incompletas do quadro do foreach
                if (taskBoardFalseElements != 0)
                {
                    TNotification notification = new TNotification();
                    notification.Title = "Tarefa(s) não-concluída(s) no quadro: " + item.Board.Name;
                    notification.Details = taskBoardFalseElements + "tarefa(s) não concluída(s) em: " + item.Board.Name;
                    TNotification.Service.Create(notification);
                }
                else
                {
                    TNotification notification = new TNotification();
                    notification.Title = "Parabéns! Você não possui tarefas pendentes";
                    notification.Details = "Parabéns, você não possui tarefa(s) não concluída(s) em: " + item.Board.Name;
                    TNotification.Service.Create(notification);
                }
            }
        }
    }
}
