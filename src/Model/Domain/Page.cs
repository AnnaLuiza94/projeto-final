﻿using System;

namespace ProjetoFinal.Domain
{
    public abstract class Page
    {
        public virtual int TotalItems { get; set; }
        public virtual int CurrentPage { get; set; }
        public virtual int ItemsPerPage { get; set; }
        public virtual int TotalPages
        {
            get
            {
                try
                {
                    if (this.TotalItems == 0)
                        return 0;
                    var division = (double)this.TotalItems / (double)this.ItemsPerPage;
                    if (division % 2 == 0)
                        return (int)division;
                    return (int)division + 1;
                }
                catch (Exception)
                {
                    return 0;
                }
            }
        }
    }
}
