using FluentValidation;
using ProjetoFinal.Domain;

namespace ProjetoFinal.Validators
{
    public class TUserValidator : AbstractValidator<TUser>
    {
        public TUserValidator()
        {
            RuleFor(x => x.Email).Must((x, email) => EmailMustBeUnique(x)).WithMessage("Este e-mail j� foi cadastrado! Por favor, escolha outro!");
           
        }

        private bool EmailMustBeUnique(TUser user)
        {
            var check = TUser.FindByEmail(user.Email);
            if (check == null)
                return true;
            if (check.Id == user.Id)
                return true;
            return false;
        }
    }
}
