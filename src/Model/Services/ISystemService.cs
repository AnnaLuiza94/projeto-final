using ProjetoFinal.Services;
using Simple.Services;
using System.Collections.Generic;
using Simple.Patterns;

namespace ProjetoFinal.Services
{
    public partial interface ISystemService : IService
    {
        IList<TaskRunner.Result> Check();
    }
}