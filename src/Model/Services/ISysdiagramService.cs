using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;

namespace ProjetoFinal.Services
{
    public partial interface ISysdiagramService : IEntityService<Sysdiagram>, IService
    {
    }
}