using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Services
{
    public partial interface ITComentService : IEntityService<TComent>, IService
    {
        TComent Create(TComent coment);
        TComent Edit(TComent coment);
        void Remove(TComent coment);
        List<TComent> ListAllcoments(Int32 idAssignment);
        TComent FindById(Int32 id);
    }
}