using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System.Collections.Generic;
using System;

namespace ProjetoFinal.Services
{
    public partial interface ITTaskService : IEntityService<TTask>, IService
    {
        TTask CreateTask(TTask task);
        TTask Create(TTask task);
        TTask Edit(TTask task);
        List<TTask> ListAllList();
        TTask FindById(Int32 id);
    }
}