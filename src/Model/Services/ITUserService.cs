using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Services
{
    public partial interface ITUserService : IEntityService<TUser>, IService
    {
        TUser Create(TUser user);
        TUser Edit(TUser user);
        void Remove(TUser user);
        List<TUser> ListAllUsers();
        List<TUser> ListAllWithActiveAccount(Int32 boardId);
        TUser ChangeProfile(TUser user, TProfile profile);
        TUser ChangePassword(Int32 id, String password);
        List<TUser> Search(String query, Int32 page, Int32 take);
        TUser FindById(Int32 id);
        TUser FindByToken(String token);
        TUser FindByEmail(String email);
        TUser FindByGoogleId(String idGoogle);
        Byte[] HashPassword(String password);
        TUser Authenticate(String email, String password);
        TUser AutenticateGoogleUser(String email, Byte[] password);
    }
}