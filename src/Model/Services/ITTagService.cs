using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System.Collections.Generic;
using System;

namespace ProjetoFinal.Services
{
    public partial interface ITTagService : IEntityService<TTag>, IService
    {
        TTag Create(TTag tag);
        List<TTag> ListAlltags();
        TTag Edit(TTag tag);
        void Remove(TTag tag);
        TTag FindById(Int32 id);
    }
}