using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Services
{
    public partial interface ITProfileService : IEntityService<TProfile>, IService
    {
        TProfile Create(TProfile profile);
        void Remove(TProfile profile);
        List<TProfile> ListAllProfiles();
        TProfile FindById(Int32 id);
    }
}