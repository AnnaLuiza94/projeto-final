using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Services
{
    public partial interface ITNotificationService : IEntityService<TNotification>, IService
    {
        TNotification Create(TNotification notification);
        void Remove(TNotification notification);
        List<TNotification> ListAllNotifications();
        TNotification FindById(Int32 id);
    }
}