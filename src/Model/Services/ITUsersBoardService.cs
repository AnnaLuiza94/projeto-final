using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;

namespace ProjetoFinal.Services
{
    public partial interface ITUsersBoardService : IEntityService<TUsersBoard>, IService
    {
        TUsersBoard Create(TUsersBoard usersBoard);
        TUsersBoard FindById(Int32 id);
    }
}