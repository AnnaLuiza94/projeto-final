using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Services
{
    public partial interface ITNoteService : IEntityService<TNote>, IService
    {
        TNote Create(TNote note);
        TNote Edit(TNote note);
        void Remove(TNote note);
        List<TNote> ListAllNotes();
        TNote FindById(Int32 id);
    }
}