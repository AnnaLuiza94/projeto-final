using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Services
{
    public partial interface ITAssignmentService : IEntityService<TAssignment>, IService
    {
        TAssignment CreateTask(TAssignment task);
        TAssignment Edit(TAssignment task);
        void Remove(TAssignment task);
        List<TAssignment> ListAllList();
        TAssignment FindById(Int32 id);
    }
}