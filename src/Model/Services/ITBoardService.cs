using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;
using System;
using System.Collections.Generic;

namespace ProjetoFinal.Services
{
    public partial interface ITBoardService : IEntityService<TBoard>, IService
    {
        TBoard Create(TBoard board);
        TBoard Edit(TBoard board);
        void Remove(TBoard board);
        void AddUsersBoard(Int32 idBoard, List<String> emails);
        TBoard ClearAllTasks(TBoard board);
        List<TBoard> ListAllByFilters();
        List<TBoard> ListAllIncludeArchiveds();
        List<TBoard> ListAllBoards();
        List<TBoard> ListAllTagBoards();
        List<TBoard> ListBoards(Int32 page, Int32 take);
        List<TBoard> ListBoardsWithTaskIncompleted(Int32 page, Int32 take);
        List<TBoard> ListBoardsByFilter(Boolean isArchived, Boolean isFavorite, Boolean isImportant, Boolean isDone, Boolean isInbox);
        Int32 CountTotalBoardsByFilters(Boolean isArchived, Boolean isFavorite, Boolean isImportant, Boolean isDone, Boolean isInbox);
        TBoard FindById(Int32 id);
    }
}