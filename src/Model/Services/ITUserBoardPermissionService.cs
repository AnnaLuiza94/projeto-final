using Simple.Entities;
using ProjetoFinal.Domain;
using Simple.Services;
using ProjetoFinal.Services;

namespace ProjetoFinal.Services
{
    public partial interface ITUserBoardPermissionService : IEntityService<TUserBoardPermission>, IService
    {
        TUserBoardPermission Create(TUserBoardPermission userBoardPermission);
    }
}