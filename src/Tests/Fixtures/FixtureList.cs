using ProjetoFinal.Tests.Fixtures.Data;
using Simple.Generator.Data;

namespace ProjetoFinal.Tests.Fixtures
{
    public class FixtureList
    {
        public static void All()
        {
            //Do<Profiles>();
            //Do<Users>();
            Do<Tags>();
        }

        public static void Test()
        {
        }

        public static void Development()
        {
        }

        #region Helper
        protected static void Do<T>()
            where T : IDataList
        {
            DataManager.Execute<T>();
        }
        #endregion
    }
}
