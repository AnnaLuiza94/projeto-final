﻿using System;
using System.Linq.Expressions;
using ProjetoFinal.Domain;
using Simple.Generator.Data;

namespace ProjetoFinal.Tests.Fixtures.Data
{
    public class Tags : DataList<TTag>
   {
       protected override void DefineItems()
       {
           var user = TUser.FindById(2);
           CreateTag("tagAdmin", "Pessoal", false, true, user);
           CreateTag("tagAdmin", "Trabalho", false, true, user);
           CreateTag("tagAdmin", "Recados", false, true, user);
           CreateTag("tagAdmin", "Compras", false, true, user);
       }

       protected void CreateTag(string chave, string nome, bool isArchived, bool fromSystem, TUser usuario)
       {
           MakeNew(chave).IdentifiedBy(x => x.Name = nome)
               .AlsoWith(x =>
               {
                   x.Name = nome;
                   x.User = usuario;
                   x.FromSystem = fromSystem;
                   x.IsArchivied = isArchived;
               });
       }

       protected override TTag OnSave(TTag model)
       {
           return base.OnSave(model);
       }

       protected override Expression<Func<TTag, bool>> FindPredicate(TTag entity)
       {
           return x => x.Name == entity.Name;
       }
    }
}
