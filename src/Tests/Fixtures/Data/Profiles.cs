﻿using System;
using System.Linq.Expressions;
using ProjetoFinal.Domain;
using Simple.Generator.Data;

namespace ProjetoFinal.Tests.Fixtures.Data
{
    public class Profiles : DataList<TProfile>
    {
        protected override void DefineItems()
        {
            CreateProfile("admin", "Administrador", "Administrador do Sistema");
            CreateProfile("user", "Usuário", "Usuário Padrão");
            CreateProfile("premium", "Usuário Premium", "Usuário pagante");
        }

        protected void CreateProfile(string chave, string nome, string descricao)
        {
            MakeNew(chave).IdentifiedBy(x => x.Name = nome)
                .AlsoWith(x =>
                {
                    x.Name = nome;
                    x.Description = descricao;
                });
        }

        protected override TProfile OnSave(TProfile model)
        {
            return base.OnSave(model);
        }

        protected override Expression<Func<TProfile, bool>> FindPredicate(TProfile entity)
        {
            return x => x.Name == entity.Name;
        }
    }
}
