﻿using System;
using System.Linq.Expressions;
using ProjetoFinal.Domain;
using Simple.Generator.Data;

namespace ProjetoFinal.Tests.Fixtures.Data
{
    public class Users : DataList<TUser>
    {
        TProfile profileAdmin = new TProfile
        {
            Id = 1,
            Name = "Administrador",
            Description = "Administrador do Sistema"
        };
        TProfile profileUser = new TProfile
        {
            Id = 2,
            Name = "Usuário",
            Description = "Usuário Padrão"

        };

        TProfile profilePremium = new TProfile
        {
            Id = 3,
            Name = "Usuário Premium",
            Description = "Usuário Premium"
        };


        protected override void DefineItems()
        {
            CreateUser("master", "Anna Luiza Medeiros", "abc@abc", "123", true, profileAdmin);
            CreateUser("admin", "Anna Luiza Medeiros", "annaluiza.medeiros@yahoo.com.br", "ProjetoFinal2015", true, profileAdmin);
            CreateUser("user", "Anna Luiza Nascimento", "annaluiza.medeiros@hotmail.com", "SimpleList2015", true, profileUser);
            CreateUser("premium", "Anna Luiza Nascimento", "annaluiiza.medeiros@gmail.com", "SimpleList2015", true, profilePremium);
        }

        protected void CreateUser(string chave, string nome, string email, string senha, bool contaAtiva, TProfile profile)
        {
            MakeNew(chave).IdentifiedBy(x => x.Email = email)
                .AlsoWith(x =>
                {
                    x.Name = nome;
                    x.Email = email;
                    x.ActivateAccount = contaAtiva;
                    x.Profile = profile;
                    x.CreationDate = DateTime.Now;
                    x.EditDate = DateTime.Now;
                    x.Password = TUser.HashPassword(senha);
                });
        }

        protected override TUser OnSave(TUser model)
        {
            return base.OnSave(model);
        }

        protected override Expression<Func<TUser, bool>> FindPredicate(TUser entity)
        {
            return x => x.Email == entity.Email;
        }
    }
}
