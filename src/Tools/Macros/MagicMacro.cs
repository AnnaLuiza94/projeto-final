using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Generator;
using ProjetoFinal.Tools.Templates.Scaffold;

namespace ProjetoFinal.Tools.Macros
{
    public class MagicMacro : ICommand
    {
        #region ICommand Members

        public void Execute()
        {
            new PrepareMacro().Execute();
            new ScaffoldGenerator().Execute();
        }

        #endregion
    }
}
