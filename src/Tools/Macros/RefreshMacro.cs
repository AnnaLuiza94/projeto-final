using ProjetoFinal.Tools.Templates.AutoContracts;
using Simple.Generator;

namespace ProjetoFinal.Tools.Macros
{
    public class RefreshMacro : ICommand
    {
        public void Execute()
        {
            new AutoContractsTemplate().Execute();
        }
    }
}
