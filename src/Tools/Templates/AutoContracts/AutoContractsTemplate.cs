using System.Diagnostics;
using Simple.Generator;

namespace ProjetoFinal.Tools.Templates.AutoContracts
{

    public class AutoContractsTemplate : ICommand
    {
        #region ICommand Members

        public void Execute()
        {
            Interfaces.HideInterfaces();
            var psi = new ProcessStartInfo("msbuild.exe", Options.Do.Solution.ProjectFile)
            {
                WorkingDirectory = Options.Do.Solution.Directory,
                UseShellExecute = false
            };
            var p = Process.Start(psi);
            p.WaitForExit();
        }


        public void Verify()
        {
            Interfaces.ShowInterfaces();
            new AutoServiceRunner().Run();
            new AutoDomainRunner().Run();
        }



        #endregion
    }
}
