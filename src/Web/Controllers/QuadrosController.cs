﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NHibernate.Linq;
using ProjetoFinal.Domain;
using ProjetoFinal.Helpers;
using ProjetoFinal.Web.Helpers;
using Simple.Web.Mvc;

namespace ProjetoFinal.Web.Controllers
{
    [RequiresAuthorization, LoadData]
    public partial class QuadrosController : Controller
    {
        public virtual ActionResult Index()
        {
            //quadros com tarefas incompletas
            // paginação var boardPage = new BoardPage(TBoard.ListBoardsWithTaskIncompleted(pagina, 3), pagina, 3, TBoard.CountTotalBoardsByFilters(false,false,false,false,true));
            var quadros = TBoard.Service.ListAllBoards();
            return View(quadros);
        }

        [HttpGet, ActionName("meus-quadros")]
        public virtual ActionResult MeusQuadros(int pagina = 1)
        {
           var boardPage = new BoardPage(TBoard.ListBoards(pagina, 3), pagina, 3, TBoard.CountTotalBoardsByFilters(false,false,false,false,false));
           //var boardPage = TBoard.Service.ListAllByFilters();
           return View(boardPage);
        }

        [HttpGet]
        public virtual ActionResult Informacoes(bool concluido, bool favorito, bool arquivado, bool importante, bool caixadeentrada, int pagina = 1)
        {
            ViewBag.Concluido = false;
            ViewBag.Favorito = false;
            ViewBag.Arquivado = false;
            ViewBag.Importante = false;
            ViewBag.CaixaDeEntrada = false;
            if(caixadeentrada)
            {
                //quadros com tarefas incompletas
                //var quadrosIncompletos = new BoardPage(TBoard.ListBoardsByFilter(pagina, 3, false, false, false, false, true), pagina, 3, TBoard.CountTotalBoardsByFilters(false, false, false, false,true));
                var quadrosIncompletos = TBoard.Service.ListBoardsByFilter(false, false, false, false, true);
                ViewBag.CaixaDeEntrada = true;
                return PartialView("_Informacoes", quadrosIncompletos);
            }
            if (concluido)
            {
                //var quadrosConcluidos = new BoardPage(TBoard.ListBoardsByFilter(pagina,3,false,false,false,true,false), pagina, 3, TBoard.CountTotalBoardsByFilters(false, false, false,true,false));
                var quadrosConcluidos = TBoard.Service.ListBoardsByFilter(false, false, false, true, false);
                ViewBag.Concluido = true;
                return PartialView("_Informacoes", quadrosConcluidos);
            }
            if (favorito)
            {
                //var quadrosFavoritos = new BoardPage(TBoard.ListBoardsByFilter(pagina, 3, false, true, false,false,false), pagina, 3, TBoard.CountTotalBoardsByFilters(false, true, false, true,false));
                var quadrosFavoritos = TBoard.Service.ListBoardsByFilter(false, true, false, false, false);
                ViewBag.Favorito = true;
                return PartialView("_Informacoes", quadrosFavoritos);
            }
            if (arquivado)
            {
                //var quadrosArquivados = new BoardPage(TBoard.ListBoardsByFilter(pagina, 3,true, false, false, false,false), pagina, 3, TBoard.CountTotalBoardsByFilters(true, false, false, false,false));
                var quadrosArquivados = TBoard.Service.ListBoardsByFilter(true, false, false, false, false);
                ViewBag.Arquivado = true;
                return PartialView("_Informacoes", quadrosArquivados);
            }
            if (importante)
            {
                //var quadrosImportantes = new BoardPage(TBoard.ListBoardsByFilter(pagina, 3, false,false, true, false,false), pagina, 3, TBoard.CountTotalBoardsByFilters(false, false, true, false,false));
                var quadrosImportantes = TBoard.Service.ListBoardsByFilter(false, false, true, false, false);
                ViewBag.Importante = true;
                return PartialView("_Informacoes", quadrosImportantes);
            }
            //incluir página com erro 404
            return View();
        }

        [HttpGet]
        public virtual ActionResult Criar()
        {
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;

            var etiquetas = TTag.Service.ListAlltags();
            ViewBag.Etiquetas = etiquetas.Count > 0 ? SelectListExtensions.ToSelectList(etiquetas, x => x.Id, x => x.Name).SelectValues() : null;
            return PartialView("_CriarQuadro");
        }

        [HttpPost]
        public virtual ActionResult Criar(TBoard quadro, string returnUrl, bool linkTag)
        {
            if (linkTag && quadro.Tag.Id != null)
                quadro.Tag = TTag.Service.FindById(quadro.Tag.Id);

            quadro.CreatedBy = SecurityContext.Do.User;
            var board = quadro.Create();

            var userBoard = new TUsersBoard();
            userBoard.User = board.CreatedBy;
            userBoard.Board = board;
            userBoard.Create();

            var permission = new TPermission();
            permission.EnumPermission = Permission.BoardAdministrator;
            permission.Name = Permission.BoardAdministrator.Description();
            permission.Create();

            var userBoardPermission = new TUserBoardPermission();
            userBoardPermission.Permission = permission;
            userBoardPermission.UserBoard = userBoard;
            userBoardPermission.Create();

            TempData["Alerta"] = new Alert("success", "Quadro criado com sucesso!");
            var notificacao = new TNotification
            {
                Title = "O quadro: "+ quadro.Name.LimitSize(25) +" foi criado.",
                User = SecurityContext.Do.User
            };
            notificacao.Create();
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction(MVC.Quadros.MeusQuadros());
            }
            return Redirect(returnUrl);
        }

        [HttpGet]
        public virtual ActionResult Gerenciar()
        {
            var quadros = TBoard.Service.ListAllByFilters();
            return View(quadros);
        }

        [HttpGet, ActionName("detalhes-quadro")]
        public virtual ActionResult Detalhes(int id)
        {
            var quadro = TBoard.FindById(id);
            return View(quadro);
        }

        [HttpGet]
        public virtual ActionResult Editar(int idQuadro)
        {
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            var etiquetas = TTag.Service.ListAlltags();
            ViewBag.Etiquetas = SelectListExtensions.ToSelectList(etiquetas, x => x.Id, x => x.Name).SelectValues();
            var quadro = TBoard.FindById(idQuadro);
            return PartialView("_Editar", quadro);
        }

        [HttpPost]
        public virtual ActionResult Editar(TBoard quadro, string returnUrl, int idTag, bool linkTag)
        {
            if (linkTag)
            {
                if (idTag != null)
                    quadro.Tag = TTag.Service.FindById(idTag);
            }

            var board = TBoard.Service.Edit(quadro);
            TempData["Alerta"] = new Alert("success", "Quadro editado com sucesso!");
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction(MVC.Quadros.MeusQuadros());
            }
            var notificacao = new TNotification
            {
                Title = "Quadro editado com sucesso!",
                User = SecurityContext.Do.User
            };
            notificacao.Create();
            return Redirect(returnUrl);
        }

        [HttpGet]
        public virtual ActionResult Excluir(int idQuadro)
        {
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            var quadro = TBoard.FindById(idQuadro);
            return PartialView("_Excluir", quadro);
        }

        public virtual ActionResult Excluir(int idQuadro, string returnUrl, bool aux = false)
        {
            var quadro = TBoard.FindById(idQuadro);
            TBoard.Service.Remove(quadro);
            TempData["Alerta"] = new Alert("success", "Quadro removido com sucesso!");
            var notificacao = new TNotification
            {
                Title = "Quadro: "+quadro.Name.LimitSize(25)+" removido com sucesso!",
                User = SecurityContext.Do.User
            };
            notificacao.Create();
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction(MVC.Quadros.MeusQuadros());
            }
            return Redirect(returnUrl);
            
        }

        public virtual ActionResult AlternarVisualizacao(int id )
        {
            var board = TBoard.FindById(id);
            return PartialView("_checklist", board);
        }

        [HttpGet]
        public virtual ActionResult AdicionarMembros(int id)
        {
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            var board = TBoard.FindById(id);
            var usuarios = TUser.Service.ListAllWithActiveAccount(board.Id);
                ViewBag.Usuarios =
                    SelectListExtensions.ToSelectList(usuarios, x => x.Email, x => x.Email).SelectValues();
                return PartialView("_AdicionarMembro", board);
        }

        [HttpPost]
        public virtual ActionResult AdicionarMembros(int id, List<string> listEmails, bool isAdministrator, string returnUrl)
        {
            if (listEmails.Any() && id != null && isAdministrator != null)
            {
                var quadro = TBoard.Service.FindById(id);
                foreach (var email in listEmails)
                {
                    var user = TUser.Service.FindByEmail(email);
                    var userBoard = new TUsersBoard
                    {
                        Board = quadro,
                        User = user
                    };
                    userBoard.Create();

                    var permission = new TPermission();
                    if (isAdministrator)
                    {
                        permission.EnumPermission = Permission.BoardAdministrator;
                        permission.Name = Permission.BoardAdministrator.Description();
                        permission.Create();
                    }
                    else permission.EnumPermission = Permission.BoardColaborator;
                         permission.Name = Permission.BoardColaborator.Description();
                         permission.Create();

                    var userPermission = new TUserBoardPermission
                    {
                        Permission = permission,
                        UserBoard = userBoard
                    };
                    userPermission.Create();

                    quadro.IsShared = true;
                    quadro.Update();

                    var notificacao = new TNotification
                    {
                        Title = "Quadro: " + quadro.Name.LimitSize(25) + " agora é um quadro compartilhado.",
                        User = SecurityContext.Do.User
                    };
                    notificacao.Create();

                }

                TempData["Alerta"] = new Alert("success", "Pronto, agora este é um quadro compartilhado.");
                if (string.IsNullOrWhiteSpace(returnUrl))
                {
                    return RedirectToAction(MVC.Quadros.MeusQuadros());
                }
                return Redirect(returnUrl);
                
            }

            else
                TempData["Alerta"] = new Alert("error", "Ocorreu algum erro ao tentar compartilhar este quadro.");

            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction(MVC.Quadros.MeusQuadros());
            }
            return Redirect(returnUrl);
        }

        [HttpGet]
        public virtual ActionResult OpcoesQuadro(int id, bool concluido, bool arquivado, bool importante, bool favorito)
        {
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            var quadro = TBoard.FindById(id);
            if (arquivado)
            {
                quadro.IsArchived = true;
                quadro.Update();
                var notificacao = new TNotification
                {
                    Title = "O quadro:  " + quadro.Name.LimitSize(25) + "foi arquivado!",
                    User = SecurityContext.Do.User
                };
                notificacao.Create();
                TempData["Alerta"] = new Alert("success", "O quadro foi arquivado!");
            }
            if (concluido)
            {
                quadro.Done = true;
                    if(quadro.TAssignments.Any())
                    foreach (var item in quadro.TAssignments)
                    {
                        item.Completed = true;
                        item.FinishedIn = DateTime.Now;
                        item.Update();
                    }
                quadro.FinishedIn = DateTime.Now;
                quadro.Update();
                TempData["Alerta"] = new Alert("success", "O quadro e suas tarefas foram marcados como concluídos!");
                
            }
            if (importante)
            {
                quadro.Important = true;
                quadro.Update();
                TempData["Alerta"] = new Alert("success", "O quadro foi marcado como importante!");
            }
            if (favorito)
            {
                quadro.Favorite = true;
                quadro.Update();
            }
            return Redirect(url.ToString());
        }

        [HttpGet]
        public virtual ActionResult OpcoesQuadroFalse(int id, bool concluido, bool arquivado, bool importante, bool favorito)
        {
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            var quadro = TBoard.FindById(id);
            if (arquivado)
            {
                quadro.IsArchived = false;
                quadro.Update();

                TempData["Alerta"] = new Alert("success", "O quadro foi desmarcado como arquivado!");
            }
            if (concluido)
            {
                quadro.Done = false;
                quadro.Update();
                TempData["Alerta"] = new Alert("success", "O quadro foi desmarcado como concluído!");
            }
            if (importante)
            {
                quadro.Important = false;
                quadro.Update();
                TempData["Alerta"] = new Alert("success", "O quadro foi desmarcado como importante!");
            }
            if (favorito)
            {
                quadro.Favorite = false;
                quadro.Update();
            }
            return Redirect(url.ToString());
        }

        public virtual ActionResult RemoverColaborador(int id)
        {
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            var userBoard = TUsersBoard.FindById(id);
            return PartialView("_ExcluirColaborador", userBoard);
        }

        [HttpPost]
        public virtual ActionResult RemoverColaborador(TUsersBoard usersBoard, int id, string returnUrl)
        {
            var userBoard = TUsersBoard.FindById(id);
            var userBoardPermission = TUserBoardPermission.Find(x => x.UserBoard == userBoard);
            userBoardPermission.Delete();
            userBoard.Delete();
            TempData["Alerta"] = new Alert("info", "Colaborador do quadro removido com sucesso.");
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction(MVC.Quadros.Gerenciar());
            }
            return Redirect(returnUrl);
        }

        [HttpGet, ActionName("ver-quadros")]
        public virtual ActionResult VerQuadros(int id)
        {
            var quadros = TBoard.ListAllByFilters();
            var tag = TTag.Service.FindById(id);
            var quadrosFiltrados = quadros.Where(x => x.Tag != null && x.Tag == tag).ToList();
            return View(quadrosFiltrados);

        }

        [HttpGet]
        public virtual ActionResult LimparTudo(int id)
        {    
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            var quadro = TBoard.FindById(id);
            return PartialView("_LimparTudo", quadro);
        }

        [HttpPost]
        public virtual ActionResult LimparTudo(int idQuadro, string returnUrl)
        {
            var quadro = TBoard.FindById(idQuadro);
            var board = TBoard.Service.ClearAllTasks(quadro);
            var notificacao = new TNotification
            {
                Title = "Você limpou todas as tarefas do quadro: " + quadro.Name.LimitSize(25)+"",
                User = SecurityContext.Do.User
            };
            notificacao.Create();
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction(MVC.Quadros.MeusQuadros());
            }
            return Redirect(returnUrl);
        }

    }
}