﻿
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ProjetoFinal.Domain;
using ProjetoFinal.Web.Helpers;

namespace ProjetoFinal.Web.Controllers
{
    [RequiresAuthorization, LoadData]
    public partial class NotasController : Controller
    {
        [HttpGet]
        public virtual ActionResult Index()
        {
            var notas = TNote.Service.ListAllNotes();
            return View(notas);
        }

        [HttpGet]
        public virtual ActionResult Criar()
        {
            return PartialView("_CriarNota");
        }

        [HttpPost]
        public virtual ActionResult Criar(TNote note)
        {
            note.User = TUser.FindById(SecurityContext.Do.User.Id);
            note.Create();
            return RedirectToAction(MVC.Notas.Index());
        }

        [HttpGet]
        public virtual ActionResult Excluir(int id)
        {
            var note = TNote.FindById(id);
            return PartialView("_Excluir", note);
        }

        public virtual ActionResult Excluir(TNote note)
        {
            TNote.Service.Remove(note);
            return RedirectToAction(MVC.Notas.Gerenciar());
        }

        [HttpGet]
        public virtual ActionResult Editar(int id)
        {
            var note = TNote.Service.FindById(id);
            return PartialView("_Editar", note);
        }


        [HttpPost]
        public virtual ActionResult Editar(TNote note)
        {
            TNote.Service.Edit(note);
            return RedirectToAction(MVC.Notas.Index());
        }


        public virtual ActionResult Gerenciar()
        {
            var notas = TNote.Service.ListAllNotes().OrderByDescending(x => x.CreationDate).ToList();
            return View(notas);
        }

        public virtual ActionResult AlterarVisualicacao()
        {
            var notas = TNote.Service.ListAllNotes().OrderByDescending(x=>x.CreationDate).ToList();
            return PartialView("_AlterarVisualizacao", notas);
        }
    }
}