﻿using System.Web.Mvc;
using ProjetoFinal.Domain;
using ProjetoFinal.Web.Helpers;

namespace ProjetoFinal.Web.Controllers
{
    [RequiresAuthorization, LoadData]
    public partial class ComentariosController : Controller
    {

        public virtual ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult AdicionarComentario(int id)
        {
            var tarefa = TAssignment.Service.FindById(id);
            return PartialView("_Adicionar", tarefa);
        }

        [HttpPost]
        public virtual ActionResult AdicionarComentario(string parametro, int id)
        {
            TComent comentario = new TComent();
            var tarefa = TAssignment.Service.FindById(id);
            var user = SecurityContext.Do.User;
            comentario.CreatedBy = user;
            comentario.Assignment = tarefa;
            comentario.Content = parametro;
            var coment = TComent.Service.Create(comentario);
            TempData["Alerta"] = new Alert("info", "Comentário adicionado com sucesso!");
            return PartialView("_comentario", coment);
        }

        [HttpGet]
        public virtual ActionResult Excluir(int id)
        {
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            var coment = TComent.Service.FindById(id);
            return PartialView("_Excluir", coment);
        }

        [HttpPost]
        public virtual ActionResult Excluir(int idComent, string returnUrl)
        {
            var comentario = TComent.Service.FindById(idComent);
            comentario.Remove();
            TempData["Alerta"] = new Alert("success", "Comentário removido com sucesso!");
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction(MVC.Quadros.Gerenciar());
            }
            return Redirect(returnUrl);
        }


        [HttpGet]
        public virtual ActionResult Editar(int id)
        {
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            var coment = TComent.Service.FindById(id);
            return PartialView("_Editar", coment);
        }

        [HttpPost]
        public virtual ActionResult Editar(TComent coment, string returnUrl)
        {
            coment.Edit();
            TempData["Alerta"] = new Alert("success", "Comentário editado com sucesso!");
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction(MVC.Quadros.Gerenciar());
            }
            return Redirect(returnUrl);
        }
    }
}