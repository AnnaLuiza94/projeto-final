using System;
using System.Linq;
using System.Web.Mvc;
using ProjetoFinal.Domain;
using ProjetoFinal.Web.Helpers;

namespace ProjetoFinal.Web.Controllers
{
    [LoadData]
    public partial class HomeController : Controller
    {
        public virtual ActionResult Index()
        {
            return View();
        }

        //[HttpGet]
        //public virtual ActionResult LoginInstitucional()
        //{
        //    return PartialView("_LoginInstitucional");
        //}

        [HttpGet]
        public virtual ActionResult AutenticacaoInstitucional(bool cadastro)
        {
            ViewBag.CadastroInstitucional = cadastro;
            return PartialView("_LoginInstitucional");
        }
    }
}
