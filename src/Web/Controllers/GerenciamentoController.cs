﻿
using System.Web.Mvc;
using ProjetoFinal.Domain;
using ProjetoFinal.Web.Helpers;
using Simple.Validation;
using Simple.Web.Mvc;

namespace ProjetoFinal.Web.Controllers
{
    [RequiresAuthorization, LoadData]
    public partial class GerenciamentoController : Controller
    {
        public virtual ActionResult Index()
        {
            var usuarios = TUser.ListAllUsers();
            return View(usuarios);
        }

        [HttpGet]
        public virtual ActionResult AlterarPefil(int idUsuario)
        {
            var usuario = TUser.FindById(idUsuario);
            ViewBag.Perfis = TProfile.ListAll().ToSelectList(x=>x.Id, x=>x.Name).SelectValues();
            return PartialView("_AlterarPerfil", usuario);
        }

        [HttpPost]
        public virtual ActionResult AlterarPerfil(int idUsuario, int idProfile)
        {
            var profile = TProfile.FindById(idProfile);
            var user = TUser.FindById(idUsuario);
            var usuario = TUser.Service.ChangeProfile(user, profile);
            TempData["Alerta"] = new Alert("success", "Perfil alterado com sucesso!");
            return RedirectToAction(MVC.Gerenciamento.Index());
        }

        [HttpGet]
        public virtual ActionResult CadastrarUsuario()
        {
            return PartialView("_CadastrarUsuario");
        }

        [HttpPost]
        public virtual ActionResult CadastrarUsuario(TUser usuario)
        {
            try
            {
                TUser.Service.Create(usuario);
                TempData["Alerta"] = new Alert("success", "Usuário cadastrado no sistema com sucesso!");
                return RedirectToAction(MVC.Gerenciamento.Index());
            }
            catch (SimpleValidationException ex)
            {
                TempData["Alerta"] = new Alert("error", "Falha na criação, verifique os dados e tente novamente.");
                return HandleViewException(usuario, ex);
            }
        }

        private ActionResult HandleViewException<T>(T model, SimpleValidationException ex)
        {
            ModelState.Clear();
            foreach (var item in ex.Errors)
            {
                ModelState.AddModelError(item.PropertyName, item.Message);
            }
            return View(model);
        }

    }
}