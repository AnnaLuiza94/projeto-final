﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Razor.Parser;
using ProjetoFinal.Domain;
using ProjetoFinal.Web.Helpers;

namespace ProjetoFinal.Web.Controllers
{
    [RequiresAuthorization, LoadData]
    public partial class EtiquetasController : Controller
    {

        public virtual ActionResult Index()
        {
            
            return View();
        }

        [HttpGet]
        public virtual ActionResult Criar()
        {
            return PartialView("_CriarEtiqueta");
        }

        [HttpPost]
        public virtual ActionResult Criar(TTag etiqueta)
        {
            etiqueta.User = TUser.FindById(SecurityContext.Do.User.Id);
            etiqueta.FromSystem = false;
            etiqueta.Create();
            return RedirectToAction(MVC.Quadros.Index());
        }

        [HttpGet]
        public virtual ActionResult Excluir(int id)
        {
            var tag = TTag.FindById(id);
            return PartialView("_Excluir", tag);
        }

        public virtual ActionResult Excluir(TTag tag)
        {
            TTag.Service.Remove(tag);
            return RedirectToAction(MVC.Quadros.Index());
        }

        [HttpGet]
        public virtual ActionResult Editar(int id)
        {
            var tag = TTag.Service.FindById(id);
            return PartialView("_Editar", tag);
        }

        public virtual ActionResult Editar(TTag tag)
        {
            var etiqueta = TTag.Service.Edit(tag);
            return RedirectToAction(MVC.Quadros.Index());
        }
    }
}