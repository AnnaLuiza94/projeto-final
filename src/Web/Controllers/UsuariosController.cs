﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using MvcContrib.UI.ReturnUrl;
using Newtonsoft.Json;
using ProjetoFinal.Domain;
using ProjetoFinal.Web.Helpers;
using Simple.Validation;
using Simple.Web.Mvc;

namespace ProjetoFinal.Web.Controllers
{
    [LoadData]
    public partial class UsuariosController : Controller
    {
        [RequiresAuthorization, LoadData]
        public virtual ActionResult Index()
        {
            var lista = TUser.ListAll().ToList();
            return View(lista);
        }

        //[HttpPost]
        //public virtual ActionResult LoginWithFacebook(string email, string name, string password, string returnUrl = null)
        //{
        //    var hashPassword = TUser.Service.HashPassword(password);
        //    TUser membroFacebook = new TUser
        //    {
        //        Name = name,
        //        Email = email,
        //        PasswordString = hashPassword.ToString(),
        //    }.Save();

        //    var membro = TUser.Authenticate(membroFacebook.Email, membroFacebook.PasswordString);
        //    if (membro != null)
        //    {
        //        FormsAuthentication.SetAuthCookie(membroFacebook.Email, true);
        //        if (string.IsNullOrWhiteSpace(returnUrl))
        //            return RedirectToAction(MVC.Home.Index());
        //        else
        //            return Redirect(returnUrl);
        //    }
        //    else
        //    {
        //        ViewBag.ReturnUrl = returnUrl ?? "";
        //        ModelState.Clear();
        //        ModelState.AddModelError("Email", "Email ou senha inválidos.");
        //        return View();
        //    }
        //}


        [HttpGet]
        public virtual ActionResult AutenticacaoGoogle(string name, string urlImage, string googleId, string returnUrl = null)
        {
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            ViewBag.Error = false;
            ViewBag.UsuarioGoogleAutenticado = false;
            var googleUser = new TUser();
            googleUser.Name = name;
            googleUser.Image = urlImage;
            googleUser.GoogleId = googleId;
            googleUser.ActivateAccount = true;

            var usuarioJaAutenticado = TUser.FindByGoogleId(googleUser.GoogleId);
            if (usuarioJaAutenticado == null)
            {
                return PartialView("_GoogleAutenticacao", googleUser);
            }
            //usuario já é cadastrado e possui um google Id
            if (usuarioJaAutenticado.GoogleId != null)
            {
                var membro = TUser.AutenticateGoogleUser(usuarioJaAutenticado.Email, usuarioJaAutenticado.Password);
                if (membro != null)
                {
                    FormsAuthentication.SetAuthCookie(usuarioJaAutenticado.Email, true);
                    ViewBag.UsuarioGoogleAutenticado = true;
                    return PartialView("_GoogleAutenticacao", membro);
                }
            }
            ViewBag.Error = true;

            //Nenhum dos casos retorna erro
            TempData["Alerta"] = new Alert("Falha na autenticação, tente novamente.", "error");
            return PartialView("_GoogleAutenticacao");

        }


        [HttpPost]
        public virtual ActionResult AutenticacaoGoogle(TUser user, string returnUrl)
        {
            var usuarioJaCadastrado = TUser.FindByEmail(user.Email);
            if (usuarioJaCadastrado != null && usuarioJaCadastrado.GoogleId == null)
            {
                //usuario cadastrado sem google Id
                usuarioJaCadastrado.GoogleId = user.GoogleId;
                if (usuarioJaCadastrado.ActivateAccount == false)
                    usuarioJaCadastrado.ActivateAccount = true;

                usuarioJaCadastrado.Update();
                //login
                var membro = TUser.Authenticate(usuarioJaCadastrado.Email, usuarioJaCadastrado.PasswordString);
                if (membro != null)
                {
                    FormsAuthentication.SetAuthCookie(usuarioJaCadastrado.Email, true);
                    return RedirectToAction(MVC.Quadros.Index());
                }
            }
            else if (usuarioJaCadastrado == null)
            {
                //Usuario que chegou não é cadastrado
                TUser.Service.Create(user);
                var membro = TUser.Authenticate(user.Email, user.PasswordString);
                if (membro != null)
                {
                    FormsAuthentication.SetAuthCookie(user.Email, true);
                    membro.ActivateAccount = true;
                    membro.Update();
                    return RedirectToAction(MVC.PaginaInicial.Index());
                }
                else
                {
                    TempData["Alerta"] = new Alert("Falha na autenticação, verifique seus dados e tente novamente.",
                        "error");
                    return Redirect(returnUrl);
                }
            }

            //usuario já é cadastrado e possui um google Id
            if (usuarioJaCadastrado.GoogleId != null)
            {
                var membro = TUser.AutenticateGoogleUser(usuarioJaCadastrado.Email, usuarioJaCadastrado.Password);
                if (membro != null)
                {
                    FormsAuthentication.SetAuthCookie(usuarioJaCadastrado.Email, true);
                    return RedirectToAction(MVC.PaginaInicial.Index());
                }
            }

            //Nenhum dos casos retorna erro
            TempData["Alerta"] = new Alert("Falha na autenticação, tente novamente.", "error");
            return Redirect(returnUrl);
        }

        [HttpGet]
        public virtual ActionResult Login(string returnUrl = null)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        public virtual ActionResult Login(TUser user, string returnUrl = null)
        {
            var membro = TUser.Authenticate(user.Email, user.PasswordString);
            if (membro != null)
            {
                FormsAuthentication.SetAuthCookie(user.Email, true);
                if (string.IsNullOrWhiteSpace(returnUrl))
                {

                    return RedirectToAction(MVC.Quadros.Index());
                }
                else
                {
                    TempData["Alerta"] = new Alert("error", "Falha na autenticação, verifique seus dados e tente novamente.");
                    return Redirect(returnUrl);
                }

            }
            TempData["Alerta"] = new Alert("error", "Falha na autenticação, verifique seus dados e tente novamente.");
            return RedirectToAction(MVC.Home.Index());
        }

        public virtual ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction(MVC.Home.Index());
        }

        [HttpGet]
        public virtual ActionResult Cadastrar()
        {
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            return RedirectToAction(MVC.Home.Index());
        }

        [HttpPost]
        public virtual ActionResult Cadastrar(TUser usuario, string returnUrl = null)
        {
            var captchaValido = !string.IsNullOrWhiteSpace(Request.Form["g-recaptcha-response"]);
            if (captchaValido)
                try
                {
                    string token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                    usuario.Token = token;
                    TUser.Service.Create(usuario);
                    var membro = TUser.Authenticate(usuario.Email, usuario.PasswordString);
                    if (membro != null)
                    {
                        FormsAuthentication.SetAuthCookie(usuario.Email, true);
                        if (string.IsNullOrWhiteSpace(returnUrl))
                            if (membro.ActivateAccount != true)
                                //new MailController().EnviarEmail(usuario).Deliver();

                        return RedirectToAction(MVC.Quadros.Index());
                        
                    }

                    ViewBag.ReturnUrl = returnUrl ?? "";
                    TempData["Alerta"] = new Alert("error",
                        "Falha na autenticação, verifique seus dados e tente novamente.");

                    return RedirectToAction(MVC.Home.Index());
                }
                catch (SimpleValidationException ex)
                {
                    return HandleViewException(usuario, ex);
                }

            TempData["Alerta"] = new Alert("error",
                    "Falha na autenticação, verifique se preencheu o captcha corretamente.");
            return RedirectToAction(MVC.Home.Index());
        }

        [HttpGet]
        public virtual ActionResult AtivarConta(int idUser)
        {
            var usuario = TUser.FindById(idUser);
            usuario.ActivateAccount = true;
            usuario.Update();
            TempData["Alerta"] = new Alert("success", "Parabéns, sua conta está ativa!");
            return RedirectToAction(MVC.Quadros.Index());
        }

        [HttpGet, ActionName("conta-ativa")]
        public virtual ActionResult ContaAtiva()
        {
            return View();
        }

        [RequiresAuthorization]
        public virtual ActionResult Excluir(int id)
        {
            var usuario = TUser.Service.FindById(id);
            return PartialView("_Excluir", usuario);
        }

        [HttpPost, RequiresAuthorization]
        public virtual ActionResult Excluir(TUser usuario)
        {
            var user = TUser.FindById(usuario.Id);
            TUser.Service.Remove(user);
            return RedirectToAction(MVC.Gerenciamento.Index());
        }

        [HttpGet, RequiresAuthorization, ActionName("minha-conta")]
        public virtual ActionResult MinhaConta()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult AlterarSenha(string token)
        {
            var usuario = TUser.Service.FindByToken(token);
            if (usuario != null)
            {
                return View(usuario);
            }
            else
            {
                TempData["Alerta"] = new Alert("error", "Ocorreu algum erro. Usuário não encontrado!");
                return RedirectToAction(MVC.PaginaInicial.Index());
            }
        }

        [HttpPost]
        public virtual ActionResult AlterarSenha(string token, string PasswordString)
        {
            var user = TUser.Service.FindByToken(token);
            if (user != null)
            {
                var users = TUser.Service.ChangePassword(user.Id, PasswordString);
                TempData["Alerta"] = new Alert("success", "Senha alterada com sucesso!");
                return RedirectToAction(MVC.PaginaInicial.Index());
            }
            else
            {
                TempData["Alerta"] = new Alert("error", "Usuário não encontrado. Tente novamente!");
                return RedirectToAction(MVC.PaginaInicial.Index());
            }
        }

        [HttpGet]
        public virtual ActionResult SolicitacaoAlteracaoSenha(bool isHome = false)
        {
            ViewBag.isHome = isHome;
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            return PartialView("_AlterarSenha");
        }

        [HttpPost]
        public virtual ActionResult SolicitacaoAlteracaoSenha(string email, string returnUrl)
        {
            var captchaValido = !string.IsNullOrWhiteSpace(Request.Form["g-recaptcha-response"]);
            if (captchaValido)
            {
                var user = TUser.Service.FindByEmail(email);
                new MailController().EmailAlteracaoSenha(user).Deliver();
                TempData["Alerta"] = new Alert("success", "Solicitação de senha enviada.");
                if (string.IsNullOrWhiteSpace(returnUrl))
                {
                    return RedirectToAction(MVC.Quadros.Index());
                }
                return Redirect(returnUrl);
            }
            else
            {
                TempData["Alerta"] = new Alert("error",
        "Falha na solicitação, verifique se preencheu o captcha corretamente.");
                return Redirect(returnUrl);
            }
        }

        [HttpGet]
        public virtual ActionResult Buscar(string parametro)
        {
            var usuarios = TUser.Service.Search(parametro, 1, 5);
            var list = new List<JsonElement>();

            foreach (var usuario in usuarios)
            {
                var element = new JsonElement(usuario);
                list.Add(element);
            }

            string json = JsonConvert.SerializeObject(list);

            return Json(json, JsonRequestBehavior.AllowGet);
        }


        [HttpGet, RequiresAuthorization, ActionName("editar-conta")]
        public virtual ActionResult EditarConta()
        {
            var usuario = SecurityContext.Do.User;
            var states = new List<string>
            {
                "Acre", "Alagoas", "Amapá", "Amazonas", "Bahia", "Ceará", "Distrito Federal", "Espírito Santo", "Goiás", "Maranhão", "Mato Grosso", "Mato Grosso do Sul", "Minas Gerais", "Pará", "Paraíba", "Paraná", "Pernambuco", "Piauí", "Rio de Janeiro", "Rio Grande do Norte", "Rio Grande do Sul", "Rondônia", "Roraima", "Santa Catarina", "São Paulo", "Sergipe", "Tocantins"
            };
            ViewBag.States = states.ToSelectList(x => x, x => x).SelectValues();
            return View(usuario);
        }

        [HttpPost, RequiresAuthorization, ActionName("editar-conta")]
        public virtual ActionResult EditarConta(TUser usuario)
        {
            var user = TUser.Service.Edit(usuario);
            try
            {
                if (Request.Files != null && Request.Files.Count > 0 && Request.Files[0].ContentLength > 0)
                {
                    var imagem = Request.Files[0];
                    string ext = Path.GetExtension(imagem.FileName);
                    string guid = Guid.NewGuid().ToString() + ext;
                    var path = Path.Combine(Server.MapPath("~/Content/upload"), guid);
                    imagem.SaveAs(path);
                    user.Image = string.Format(Request.IsLocal ? "http://localhost/projetofinal/Content/upload/{0}" : "http://www.simpletasker.com.br/Content/upload/{0}", guid); // quando o site subir alterar o segundo endereco para o endereco real
                }
            }
            catch (Exception)
            {
                TempData["Alerta"] = new Alert("erro", "Ocorreu algum erro ao tentar salvar a imagem do seu perfil.");
                throw;
            }
            TempData["Alerta"] = new Alert("success", "Dados alerados com sucesso.");
            return RedirectToAction(MVC.Usuarios.EditarConta());
        }



        private ActionResult HandleViewException<T>(T model, SimpleValidationException ex)
        {
            ModelState.Clear();
            foreach (var item in ex.Errors)
            {
                ModelState.AddModelError(item.PropertyName, item.Message);
            }
            return View(model);
        }
    }
}