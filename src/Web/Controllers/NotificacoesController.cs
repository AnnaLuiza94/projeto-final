﻿using System.Linq;
using System.Web.Mvc;
using ProjetoFinal.Domain;
using ProjetoFinal.Web.Helpers;

namespace ProjetoFinal.Web.Controllers
{
    [RequiresAuthorization, LoadData]
    public partial class NotificacoesController : Controller
    {
        public virtual ActionResult Index()
        {
           var notifications = TNotification.Service.ListAllNotifications().OrderByDescending(x=>x.CreationDate);
           return View(notifications);
        }
    }
}