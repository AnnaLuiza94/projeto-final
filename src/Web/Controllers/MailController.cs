﻿using ActionMailer.Net.Mvc;
using ProjetoFinal.Domain;
using ProjetoFinal.Web.Helpers;

namespace ProjetoFinal.Web.Controllers
{
    public partial class MailController : MailerBaseWithoutCertificateValidation
    {
        public virtual EmailResult EnviarEmail(TUser user)
        {
            To.Add(user.Email);
            From = "annaluiza.medeiros@yahoo.com.br";
            Subject = "Ativar Conta em SimpleTasker";
            return Email("EnviarEmail", user);
        }

        public virtual EmailResult EmailAlteracaoSenha(TUser user)
        {
            To.Add(user.Email);
            From = "simpletaskercompany@gmail.com";
            Subject = "Solicitação de Alteração de Senha em SimpleTasker";
            return Email("EmailAlterarSenha", user);
        }
    }
}