﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using NHibernate.Hql.Ast.ANTLR.Tree;
using ProjetoFinal.Domain;
using ProjetoFinal.Web.Helpers;
using Simple.Web.Mvc;

namespace ProjetoFinal.Web.Controllers
{
    [RequiresAuthorization, LoadData]
    public partial class TarefasController : Controller
    {
        public virtual ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public virtual ActionResult Criar(int idQuadro)
        {

            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            var board = TBoard.FindById(idQuadro);
            return PartialView("_AdicionarTarefa", board.Id);
        }

        [HttpPost]
        public virtual ActionResult Criar(TAssignment tarefa, int idBoard, string returnUrl)
        {
            tarefa.Board = TBoard.FindById(idBoard);
            tarefa.EndDate = tarefa.EndDate;
            TAssignment.Service.CreateTask(tarefa);
            tarefa.Save();
            TempData["Alerta"] = new Alert("success", "Parabéns, você tem uma nova tarefa");
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction(MVC.Quadros.MeusQuadros());
            }
            return Redirect(returnUrl);
        }

        [HttpGet]
        public virtual ActionResult Editar(int idTarefa)
        {
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            var tarefa = TAssignment.Service.FindById(idTarefa);
            return PartialView("_EditarTarefa", tarefa);
        }

        [HttpPost]
        public virtual ActionResult Editar(TAssignment tarefa, int idBoard, string returnUrl)
        {
            tarefa.Board = TBoard.FindById(idBoard);
            tarefa.EndDate = tarefa.EndDate;
            var task = TAssignment.Service.Edit(tarefa);
            TempData["Alerta"] = new Alert("success", "Tarefa editada com sucesso!");
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction(MVC.Quadros.MeusQuadros());
            }
            return Redirect(returnUrl);
        }

        [HttpGet]
        public virtual ActionResult Remover(int idTarefa)
        {
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            var tarefa = TAssignment.Service.FindById(idTarefa);
            return PartialView("_ExcluirTarefa", tarefa);
        }

        [HttpPost]
        public virtual ActionResult Remover(int idTarefa, string returnUrl, bool aux = false)
        {
            var tarefa = TAssignment.Service.FindById(idTarefa);
            tarefa.Remove();
            TempData["Alerta"] = new Alert("success", "Tarefa removida com sucesso!");
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction(MVC.Quadros.MeusQuadros());
            }
            return Redirect(returnUrl);
        }

        public virtual ActionResult Detalhes(int idTarefa)
        {
            var tarefa = TAssignment.Service.FindById(idTarefa);
            ViewBag.Comentarios = TComent.ListAllcoments(tarefa.Id);
            return PartialView("_Detalhes", tarefa);
        }

        public virtual ActionResult Calendario()
        {
            var quadros = TBoard.Service.ListAllByFilters();
            var tarefas = new List<TAssignment>();
            foreach (var quadro in quadros)
                tarefas.AddRange(quadro.TAssignments.ToList());
            return View(tarefas);
        }

        [HttpPost]
        public virtual ActionResult TrocarQuadro(int idTarefa, int idQuadro)
        {
            var tarefa = TAssignment.Service.FindById(idTarefa);
            var novoQuadro = TBoard.Service.FindById(idQuadro);
            tarefa.Board = novoQuadro;
            tarefa.Update();
            return Content("Quadro movido com sucesso!");
        }

        [HttpGet]
        public virtual ActionResult AnexarArquivo()
        {
            return PartialView("_Anexar");
        }

        public virtual ActionResult ConcluirTarefa(int id, bool concluido)
        {
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            var tarefa = TAssignment.FindById(id);
            tarefa.Completed = concluido;
            tarefa.Update();
            TempData["Alerta"] = new Alert("success", concluido ? "Tarefa foi concluida!" : "Tarefa foi marcada como incompleta");
            return Redirect(url.ToString());
        }

        [HttpGet]
        public virtual ActionResult TarefaRapida()
        {
            // get the previous url and store it with view model
            var url = System.Web.HttpContext.Current.Request.UrlReferrer;
            ViewBag.ReturnUrl = url;
            var quadros = TBoard.Service.ListAllByFilters();
            ViewBag.Quadros = quadros.ToSelectList(x => x.Id, x => x.Name).SelectValues();
            return PartialView("_TarefaRapida");
        }
    }
}