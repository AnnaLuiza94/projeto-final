using System;
using System.Linq;
using System.Web.Mvc;
using ProjetoFinal.Domain;
using ProjetoFinal.Web.Helpers;

namespace ProjetoFinal.Web.Controllers
{
    [RequiresAuthorization, LoadData]
    public partial class PaginaInicialController : Controller
    {
        public virtual ActionResult Index()
        {
            var quadros = TBoard.ListAllBoards();
            ViewBag.NotesElements = TNote.Service.ListAllNotes();
            return View(quadros);

        }
    }
}
