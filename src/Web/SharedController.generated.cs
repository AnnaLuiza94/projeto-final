// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
#pragma warning disable 1591, 3008, 3009, 0108
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace T4MVC
{
    public class SharedController
    {

        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string _Alertas = "_Alertas";
                public readonly string _Confirm = "_Confirm";
                public readonly string _Layout = "_Layout";
                public readonly string _LayoutInstitucional = "_LayoutInstitucional";
                public readonly string _LayoutLogin = "_LayoutLogin";
                public readonly string _LayoutOptional = "_LayoutOptional";
                public readonly string _MenuLateral = "_MenuLateral";
                public readonly string _Notificacoes = "_Notificacoes";
                public readonly string _ResizeImage = "_ResizeImage";
                public readonly string _RightSidebar = "_RightSidebar";
                public readonly string _ScriptsFacebook = "_ScriptsFacebook";
                public readonly string _SharedScripts = "_SharedScripts";
                public readonly string Error = "Error";
            }
            public readonly string _Alertas = "~/Views/Shared/_Alertas.cshtml";
            public readonly string _Confirm = "~/Views/Shared/_Confirm.cshtml";
            public readonly string _Layout = "~/Views/Shared/_Layout.cshtml";
            public readonly string _LayoutInstitucional = "~/Views/Shared/_LayoutInstitucional.cshtml";
            public readonly string _LayoutLogin = "~/Views/Shared/_LayoutLogin.cshtml";
            public readonly string _LayoutOptional = "~/Views/Shared/_LayoutOptional.cshtml";
            public readonly string _MenuLateral = "~/Views/Shared/_MenuLateral.cshtml";
            public readonly string _Notificacoes = "~/Views/Shared/_Notificacoes.cshtml";
            public readonly string _ResizeImage = "~/Views/Shared/_ResizeImage.cshtml";
            public readonly string _RightSidebar = "~/Views/Shared/_RightSidebar.cshtml";
            public readonly string _ScriptsFacebook = "~/Views/Shared/_ScriptsFacebook.cshtml";
            public readonly string _SharedScripts = "~/Views/Shared/_SharedScripts.cshtml";
            public readonly string Error = "~/Views/Shared/Error.cshtml";
        }
    }

}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108
