﻿using System.Web.Mvc;

namespace ProjetoFinal.Web.Helpers
{
    public class LoadDataAttribute : ActionFilterAttribute
    {
        public LoadDataAttribute()
        {
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var viewResult = filterContext.Controller;

            if (viewResult == null)
                return;

            var userIdentity = filterContext.HttpContext.User.Identity;

            viewResult.ViewBag.Alerta = viewResult.TempData["Alerta"];
            viewResult.ViewBag.Imagem = viewResult.TempData["Imagem"];
            viewResult.ViewBag.MenuTab = viewResult.TempData["MenuTab"];
            viewResult.ViewBag.SubMenuTab = viewResult.TempData["SubMenuTab"];

            base.OnActionExecuting(filterContext);
        }
    }
}