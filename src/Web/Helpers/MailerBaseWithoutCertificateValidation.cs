﻿using ActionMailer.Net;
using ActionMailer.Net.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ProjetoFinal.Web.Helpers
{
    public class MailerBaseWithoutCertificateValidation : MailerBase
    {
        protected override void OnMailSending(MailSendingContext context)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            base.OnMailSending(context);
        }
    }
}