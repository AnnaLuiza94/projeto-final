﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using ProjetoFinal.Domain;

namespace ProjetoFinal.Web.Helpers
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class RequiresAuthorizationAttribute : ActionFilterAttribute, IAuthorizationFilter, IExceptionFilter
    {
        public RequiresAuthorizationAttribute()
        {
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            SecurityContext.Do.Init(() => filterContext.HttpContext.User.Identity);

            string actionName = filterContext.ActionDescriptor.ActionName;
            string controllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            var security = SecurityContext.Do;

            bool isLoggedIn = security.IsAuthenticated;

            if (!isLoggedIn)
            {
                NaoLogado(filterContext);
                return;
            }

            base.OnActionExecuting(filterContext);
        }

        private static void NaoLogado(ActionExecutingContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                    { "controller", "Home" }, 
                    { "action", "Index" } 
                });
        }

        private static void AcessoNegado(ExceptionContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                    { "controller", "Home" }, 
                    { "action", "Index" } 
                });
        }

        private static void AcessoNegado(ActionExecutingContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary {
                    { "controller", "Home" }, 
                    { "action", "Index" } 
                });
        }

        public void OnAuthorization(AuthorizationContext filterContext)
        {
        }

        #region IExceptionFilter Members

        public void OnException(ExceptionContext filterContext)
        {
            if (filterContext.Exception is AuthorizationException)
                AcessoNegado(filterContext);
        }

        #endregion
    }
}
