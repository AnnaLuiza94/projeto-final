﻿var App = function () {
    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    var handleScrollPane = function () {
        $(".scroll-pane").livequery(function () {
            $(this).jScrollPane();
        });
    }


    function processAjaxResult(data, form, cleanForm, append, replace) {
        var isJsonResult = false;
        if (isJson(data)) {
            data = JSON.parse(data);
            isJsonResult = true;
            if (data["Alert"] != null && data["Alert"] != "") {
                App.initAddAlertToPage(data["Alert"]["Message"], data["Alert"]["Type"]);
            }
        }

        if (cleanForm != null && cleanForm == "true") {
            $(form).find("input:not([type='hidden']), textarea").val("");
        }
        if (append != null && append != "") {
            if (!isJsonResult) {
                console.info("not json");
                $(append).append(data);
            } else {
                if (data["Content"] != null) {
                    $(append).append(data["Content"]);
                }
            }
            if ($(append).parents(".scroll-pane").length > 0) {
                $(append).parents(".scroll-pane").jScrollPane().data("jsp").scrollToBottom();
            }
        }
        if (replace != null && replace != "") {
            if (!isJsonResult) {
                $(replace).empty().html(data);
            } else {
                if (data["Content"] != null && data["Content"] != "") {
                    $(replace).empty().html(data["Content"]);
                }
            }
            if ($(replace).parents(".scroll-pane").length > 0) {
                $(replace).parents(".scroll-pane").jScrollPane().data("jsp").scrollToBottom();
            }
            if ($(".modal").length > 0) {
                App.openCustomModal();
            }
        }
    }



    var handleForm = function () {
        $.validator.addMethod("alphanumeric", function (value, element) {
            return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
        }, "Utilize apenas letras e números.");

        $.validator.addMethod("lettersAndNumbers", function (value, element) {
            return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
        }, "É necessário conter letras e números.");

        $.extend($.validator.messages, {
            equalTo: "As senhas digitadas devem ser iguais."
        });

        $(".form-register").livequery(function () {
            var equalToPassword = $("#PasswordString");
            $(this).validate({
                ignore: [],
                rules: {
                    email: {
                        required: true
                    },
                    PasswordString: {
                        required: true,
                        minlength: 6,
                        alphanumeric: true,
                        lettersAndNumbers: true
                    },
                    Password: {
                        required: true,
                        minlength: 6,
                        alphanumeric: true,
                        lettersAndNumbers: true
                    },
                    senhaAtual: {
                        required: true,
                        minlength: 6,
                        alphanumeric: true,
                        lettersAndNumbers: true
                    },
                    confirmaSenha: {
                        required: true,
                        minlength: 6,
                        alphanumeric: true,
                        equalTo: equalToPassword,
                        lettersAndNumbers: true
                    },
                    novaSenha: {
                        required: true,
                        minlength: 6,
                        alphanumeric: true,
                        lettersAndNumbers: true
                    }
                }
            });
        });

        $(".form-ajax").livequery(function () {
            var cleanForm = $(this).attr("data-cleanform");
            var append = $(this).attr("data-append");
            var replace = $(this).attr("data-replace");
            $(this).validate({
                errorPlacement: function (error, element) {
                    if (!element.hasClass("no-error-label")) {
                        element.parent().append(error);
                    }
                },
                submitHandler: function (form) {
                    //App.blockUI({ boxed: true });
                    $.ajax({
                        type: "POST",
                        url: $(form).attr("action"),
                        data: $(form).serialize(),
                        success: function (data) {
                            //App.unblockUI();
                            processAjaxResult(data, form, cleanForm, append, replace);
                        }
                    });

                    return false;
                }
            });
        });
    }

    var handleAjaxRequest = function () {
        $(".ajax-request").livequery(function () {
            $(this).click(function (e) {
                e.preventDefault();
                var type = "GET";
                if ($(this).hasClass("post")) {
                    type = "POST";
                }

                if (url == null || url == "" || url == "#")
                    return false;
                else {
                    //App.blockUI({ boxed: true });
                    $.ajax({
                        type: type,
                        url: url,
                        cache: false,
                        success: function (data) {
                            //App.unblockUI();
                            //App.initAddAlertToPage(data["message"], data["type"]);
                        }
                    });
                }
            });
        });

        var doAjaxRequestContainer = function (element, e, parametro, alternativeAjax) {
            if (e != null)
                e.preventDefault();
            if (element.hasClass("no-more-ajax"))
                return false;
            var noMoreAjax = element.hasClass("ajax-once");
            var type = "GET";
            if (element.hasClass("post")) {
                type = "POST";
            }
            var container = element.attr("data-container");
            if (container == null || container == "")
                container = element.attr("data-id");
            var url = element.attr('href');
            if (url == null || url == "")
                url = element.attr("data-url");
            if (url == null || url == "" || url == "#" || container == null || container == "")
                return false;
            else {
                console.info(type);
                //App.blockUI({ boxed: true });
                if (alternativeAjax) {
                    if (type == "POST") {
                        $.post(url, { parametro: parametro })
                            .done(function (data) {
                                if (data == null || data == false)
                                    return;
                                if (data == "invalid")
                                    $(container).empty().html(data);

                                //App.unblockUI();
                                //App.initAddAlertToPage(data["message"], data["type"]);

                                if (noMoreAjax) {
                                    $(element).addClass("no-more-ajax");
                                }

                                if (!isJson(data)) {
                                    $(container).empty().html(data);
                                    if ($(".modal").length > 0) {
                                        App.openCustomModal();
                                    }
                                } else {
                                    processAjaxResult(data, null, null, null, container);
                                }
                            });
                    } else {
                        $.get(url, { parametro: parametro })
                            .done(function (data) {
                                if (data == null || data == false)
                                    return;
                                if (data == "invalid")
                                    $(container).empty().html(data);

                                //App.unblockUI();
                                //App.initAddAlertToPage(data["message"], data["type"]);

                                if (noMoreAjax) {
                                    $(element).addClass("no-more-ajax");
                                }

                                if (!isJson(data)) {
                                    $(container).empty().html(data);
                                    if ($(".modal").length > 0) {
                                        App.openCustomModal();
                                    }
                                } else {
                                    processAjaxResult(data, null, null, null, container);
                                }
                            });
                    }
                } else {
                    $.ajax({
                        type: type,
                        url: url,
                        cache: false,
                        data: parametro == null || parametro == "" ? "" : "parametro=" + parametro,
                        success: function (data) {
                            if (data == null || data == false)
                                return;

                            if (data == "invalid")
                                $(container).empty().html(data);

                            //App.unblockUI();
                            //App.initAddAlertToPage(data["message"], data["type"]);

                            if (noMoreAjax) {
                                $(element).addClass("no-more-ajax");
                            }

                            if (!isJson(data)) {
                                $(container).empty().html(data);
                                if ($(".modal").length > 0) {
                                    App.openCustomModal();
                                }
                            } else {
                                processAjaxResult(data, null, null, null, container);
                            }
                        }
                    });
                }
            }
        }

        $(".ajax-request-container").livequery(function () {
            $(this).click(function (e) {
                var parametro = $("#content").val();
                if (parametro != null)
                    doAjaxRequestContainer($(this), e, parametro, false);
                else {
                    doAjaxRequestContainer($(this), e, null, false);
                }
            });
        });

        $(".ajax-request-container-select").livequery(function () {
            $(this).change(function () {
                var estado = $(this).find("option:selected").text();
                $("#State").val(estado);
                var parametro = $(this).val();
                doAjaxRequestContainer($(this), null, parametro, false);
            });
        });

        $(".ajax-request-container-form").livequery(function () {
            $(this).click(function () {
                var parametro = $(this).parents("form").find(".ajax-request-container-form-parameter").val().replace(/\r?\n/g, "\r\n");
                doAjaxRequestContainer($(this), null, parametro, true);
            });
        });

        $("#cad-cidade").livequery(function () {
            $(this).change(function () {
                var cidade = $(this).find("option:selected").text();
                $("#City").val(cidade);
            });
        });
    }

    var handleValidateCaptcha = function () {
        $("#g-recaptcha-response").livequery(function () {
            $(this).addClass("required");
        });
    }

    var handleClick = function () {
        $("body").livequery(function () {
            $(this).click(function (e) {
                var element = $(e.target);
                if (!element.hasClass("handle-click")) {
                    element = element.parents(".handle-click");
                }
                if (element.length > 0) {
                    var classToAdd = element.attr("data-active-class");
                    if (classToAdd != null && classToAdd != "") {
                        $(".handle-click").each(function () {
                            $(this).removeClass($(this).attr("data-active-class"));
                        });

                        element.addClass(classToAdd);
                    }
                }
            });
        });
    }

    var handleMask = function () {
        $('input[type=text]').livequery(function () {
            $(this).setMask();
            $.mask.masks = $.extend($.mask.masks, {
                phone_9digito: { mask: '(99) 99999-9999' }
            });
        });
    }

    var handleFileUploader = function () {
        $(".btn-fileuploader").livequery(function () {
            $(this).click(function () {
                console.info("click");
                var element = $(this);
                console.info($(element).attr("data-uploadfile"));
                if ($(element).attr("data-uploadfile") == null || $(element).attr("data-uploadfile") == "") {
                    var form = $(this).parents("form");
                    var fileUploader = $(form).find(".fileuploader");
                    var url = $(form).attr("action");
                    var data = $(form).serialize();
                    var submitButton = $(form).find(".btn-enviar");
                    var id = $(form).find("[name=id]").val();
                    var replace = $(element).attr("data-replace");
                    var uploadFile = $(fileUploader).uploadFile({
                        url: url,
                        fileName: "myfile",
                        autoSubmit: true,
                        formData: { "id": id },
                        multiple: false,
                        maxFileSize: 1024 * 1024 * 5,
                        showAbort: false,
                        showDone: false,
                        allowedTypes: "jpg,jpeg,mp3,png,pdf,doc,docx,ppt,pptx,pps,xls,xlsx,zip,rar",
                        onSuccess: function (files, data, xhr) {
                            processAjaxResult(data, form, null, null, replace);
                        }
                    });

                    var inputUploadFile = $(form).find(".ajax-file-upload").find("input");
                    $(element).attr("data-uploadfile", $(inputUploadFile).attr("id"));
                    $(inputUploadFile).click();

                    //$(submitButton).click(function() {
                    //    uploadFile.startUpload();
                    //});
                } else {
                    console.info("aqui");
                    var fileUploadId = $(element).attr("data-uploadfile");
                    $("#" + fileUploadId).click();
                }
            });
        });

        //$(".form-upload").livequery(function () {
        //    $(this).submit(function(e) {
        //        e.preventDefault();
        //        return false;
        //    });
        //});

        $(".form-upload").livequery(function () {
            var idAvaliacao = $(this).find("[name=idAvaliacao]").val();
            var chave = $(this).find("[name=chave]").val();
            var validateUrl = $(this).attr("data-validate-url");
            var replace = $(this).attr("data-replace");
            var form = $(this);
            $(this).fileUpload({
                uploadData: { "idAvaliacao": idAvaliacao, "chave": chave, "texto": "", "idioma": "pt" },
                error: function (jqXHR, textStatus, errorThrown) {
                    //XML no retorno é tratado como erro nesse plugin
                    $.ajax({
                        type: "POST",
                        url: validateUrl,
                        cache: false,
                        data: "id=" + idAvaliacao + "&retorno=True",
                        success: function (response) {
                            processAjaxResult(response, form, null, null, replace);
                        }
                    });
                }
            });
        });

        $(".btn-upload").livequery(function () {
            $(this).click(function () {
                var element = $(this);
                var form = $(this).parents(".form-upload");
                var file = $(this).parents(".group-file-upload").find(".hidden-upload");
                $(file).click();
                $(file).change(function () {
                    //console.info("mudou");
                    $(form).submit();
                    $(element).attr("disabled", "disabled").empty().html("CARREGANDO...");
                });
            });
        });
    }

    var addAlertToPage = function (message, type) {
        if (!toastr) {
            return;
        }

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1000",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        toastr.options.progressBar = true;
        var $toast = toastr[type](message, null);

        return false;
    }

    var handleValidate = function () {
        $(".form-validate").livequery(function () {
            $(this).validate({
                errorPlacement: function (error, element) {
                    if (!element.hasClass("no-error-label")) {
                        element.parent().append(error);
                    }
                }
            });
        });
    }

    var addResizeImage = function(heightImage, widthImage) {
        $(".resizeImage").livequery(function() {
            $(this).aeImageResize({ height: heightImage, width: widthImage });
        });
    }

    var getSiteRoot = function () {
        var rootPath = window.location.protocol + "//" + window.location.host + "/";
        var pathArray = window.location.pathname.split('/');
        if (window.location.hostname == "localhost" || pathArray[0] == "projetofinal" || pathArray[1] == "projetofinal") {
            var path = window.location.pathname;
            if (path.indexOf("/") == 0) {
                path = path.substring(1);
            }
            path = path.split("/", 1);
            if (path != "") {
                rootPath = rootPath + path + "/";
            }
        }

        return rootPath;
    }


    return {
        init: function () {
            handleScrollPane();
            handleAjaxRequest();
            handleMask();
            handleValidate();
            handleForm();
            handleValidateCaptcha();
            handleClick();
            handleFileUploader();
        },

        initAddAlertToPage: function (message, type) {
            addAlertToPage(message, type);
        },

        initAddResizeImage: function (heightImage, widthImage) {
            addResizeImage(heightImage, widthImage);
        },

        blockUI: function (options) {
            var options = $.extend(true, {}, options);
            var html = '';
            var siteRoot = getSiteRoot();
            if (options.iconOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img style="" src="' + siteRoot + 'Content/img/ajax-loader.gif" align=""></div>';
            } else if (options.textOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><span>&nbsp;&nbsp;' + (options.message ? options.message : 'Carregando...') + '</span></div>';
            } else {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img style="" src="' + siteRoot + 'Content/img/ajax-loader.gif" align=""><span>&nbsp;&nbsp;' + (options.message ? options.message : 'Carregando...') + '</span></div>';
            }

            if (options.target) {
                var el = jQuery(options.target);
                if (el.height() <= ($(window).height())) {
                    options.cenrerY = true;
                }
                el.block({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    centerY: options.cenrerY != undefined ? options.cenrerY : false,
                    css: {
                        top: '10%',
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#000',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            } else {
                $.blockUI({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    css: {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#000',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            }
        },

        unblockUI: function (target) {
            if (target) {
                jQuery(target).unblock({
                    onUnblock: function () {
                        jQuery(target).css('position', '');
                        jQuery(target).css('zoom', '');
                    }
                });
            } else {
                $.unblockUI();
            }
        },

        openCustomModal: function () {
            $(".modal").fadeIn(function () {
                handleScrollPane();
                $(".modal").find(".btn-fechar").click(function () {
                    $(".modal").fadeOut();
                    $(".modal").remove();
                });
            });
        }
    };
}();