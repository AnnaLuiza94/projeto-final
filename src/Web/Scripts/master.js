﻿
$(".ajax-request-container").livequery(function () {
    $(this).click(function (e) {
        e.preventDefault();
        var element = $(this);
        if (element.hasClass("no-more-ajax"))
            return false;
        var noMoreAjax = $(this).hasClass("ajax-once");
        var type = "GET";
        if ($(this).hasClass("post")) {
            type = "POST";
        }
        var container = $(this).attr("data-container");
        if (container == null || container == "")
            container = $(this).attr("data-id");
        if (container == null || container == "")
            container = $(".input-group-btn").prev("input[type=text]").val();
        var url = $(this).attr("href");
        if (url == null || url == "" || url == "#" || container == null || container == "")
            return false;
        else {
            //App.blockUI({ boxed: true });
            $.ajax({
                type: type,
                url: url,
                data: container,
                cache: false,
                success: function (data) {
                    //App.unblockUI();
                    //App.initAddAlertToPage(data["message"], data["type"]);
                    if (noMoreAjax) {
                        $(element).addClass("no-more-ajax");
                    }
                    $(container).empty().html(data);
                    if ($(".modal").length > 0) {
                        App.openCustomModal();
                    }
                }
            });
        }
    });
});

$(".add-task-agile-box").livequery(function () {
    $(this).click(function (e) {
        e.preventDefault();
        var content = $('.input-group-btn').prev('input[type=text]').val();
        $.ajax({
            url: './Tarefas/AdicionarTarefa',
            type: 'GET',
            data: { 'conteudo': content },
            success: function (data) {
                $('#to-do').html(data);
            }
        });
    });
});

$(".ajax-autocomplete").livequery(function () {
    var input = $(this);
    var address = input.attr("data-url");
    var map = {};

    $(this).typeahead({
        source: function (query, process) {
            console.info(query);
            map = {};
            return $.get(address, { parametro: query }, function (data) {
                var json = jQuery.parseJSON(data);
                var newData = [];
                $.each(json, function (i, obj) {
                    map[obj.text] = obj;
                    newData.push(obj.text);
                });

                return process(newData);
            });
        },
        updater: function (item) {
            input.parents(".control-group").find(".hidden-json-id").val(map[item].id);
            return item;
        }
    });

    $(this).blur(function () {
        var check = $(this).parents(".control-group").find(".hidden-json-id").val();
        if (check == null || check == "") {
            $(this).val("");
        }
    });
});

$(".tool-tip").livequery(function () {
    $(this).tooltip({
        placement: $(this).attr("data-placement"),
        container: "body"
    });
});

$(".add-list").livequery(function () {
    $(this).click(function (e) {
        e.preventDefault();

        $.ajax({
            url: './Lista/Cadastrar',

            type: "GET",

            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                $("#new-list").empty().html(data);
            },
            error: function (xhr) {
                alert('error');
            }
        });

    });
});

$(".remove-list").livequery(function () {
    $(this).click(function (e) {
        e.preventDefault();

        $.ajax({
            url: './Lista/Cadastrar',

            type: "GET",

            async: true,
            processData: false,
            cache: false,
            success: function (data) {
                $("#new-list").empty().html(data);
            },
            error: function (xhr) {
                alert('error');
            }
        });

    });
});

$(".ajax-modal").livequery(function () {
    $(this).click(function (e) {
        e.preventDefault();

        var url = $(this).attr("href");
        if (url != null && url != "") {
            App.blockUI({ boxed: true });
            $.ajax({
                url: url,
                type: "GET",
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                    App.unblockUI();
                    if (data != false) {
                        $("#modal").empty().html(data);
                        $("#myModal").modal("show");
                    }
                }
            });
        }
    });
});

    $(".show-manager").livequery(function() {
        $(this).mouseover(function () {
              $(".remove-hidden").removeClass("hidden");
        }).mouseout(function () {
            $(".remove-hidden").addClass("hidden");
        });
    });
     

    $("#datepicker .input-group.date").datepicker({
        todayBtn: "linked",
        orientation: "bottom",
        keyboardNavigation: false,
        forceParse: false,
        format: "dd/mm/yyyy",
        startDate: "-3d",
        calendarWeeks: true,
        autoclose: true,
        onSelect: function(date, instance) {
            console.info(date);
            console.info(instance);
        }
    });






    var $datepicker = $("#datepicker");
    $datepicker.datepicker();
    $datepicker.datepicker("setDate", new Date());


    $(".ajax-partial-page").livequery(function () {
        $(this).click(function (e) {
            e.preventDefault();
            var modal = false;
            if ($(".ajax-partial-page").hasClass("modal-ajax")) {
                modal = true;
            }
            var container = $("#data-container");
            if (modal == true) {
                container = $("#data-container-modal");
            }
            var url = $(this).attr("href");
            var id = $("data-id").val();
            var type = "GET";
            if ($(this).hasClass("post")) {
                type = "POST";
            }
            if (url != null && url != "") {
                $.ajax({
                    url: url,
                    type: type,
                    async: true,
                    processData: false,
                    cache: false,
                    success: function (data) {
                        if (data != false) {
                            container.empty().html(data);
                        }
                    }
                });
            }
        });
    });

        $(".ajax-request-coment").livequery(function () {
            $(this).click(function (e) {
                e.preventDefault();
                var url = $(this).attr("href");
                var type = "GET";
                if ($(this).hasClass("post")) {
                    type = "POST";
                }
                //var element = $(this).parents("row").find("#container-comentario");
                //console.info(element);
                //var container = element.attr("#data-container", "comentario-container");

                var container = $("#container-comentario");
                
                var content = $("#content").val();
                if (type == "POST") {
                    $.post(url, { parametro: content })
                        .done(function (data) {
                            if (data != false) {
                                container.empty().html(data);
                            }
                        });

                }
            });
        });



var replaceWith = $('<input id="Name" type="text"  name="Name" class="hiddenField" />'),
    connectWith = $('input[name="hiddenFieldName"]');
    $(".inline-editor-name").livequery(function(){
        $(this).hover(function () {
            $(this).addClass('hover');
        }, function () {
            $(this).removeClass('hover');
        });

        $(this).click(function () {
            var elem = $(this);
            elem.hide();
            elem.after(replaceWith);
            replaceWith.focus();

            replaceWith.blur(function () {

                if ($(this).val() != "") {
                    connectWith.val($(this).val()).change();
                    elem.text($(this).val());
                }

                //$(this).remove();
                //elem.show();
            });
        });

    });

var replaceWith = $('<input id="Content"  name="Content" type="text"  class="hiddenField" />'),
    connectWith = $('input[name="hiddenFieldContent"]');
    $(".inline-editor-content").livequery(function () {
        $(this).hover(function () {
            $(this).addClass('hover');
        }, function () {
            $(this).removeClass('hover');
        });

        $(this).click(function () {
            var elem = $(this);
            elem.hide();
            elem.after(replaceWith);
            replaceWith.focus();

            replaceWith.blur(function () {

                if ($(this).val() != "") {
                    connectWith.val($(this).val()).change();
                    elem.text($(this).val());
                }

                //$(this).remove();
                //elem.show();
            });
        });

    });
    
    //$("#side-menu").mouseleave(function () {
    //    $("body").toggleClass("mini-navbar");
    //    SmoothlyMenu();

    //});

$(".icheckbox_square-green").livequery(function() {
    $(this).click(function() {
        var url = $(this).attr("href");
        var type = "POST";
        if (url != null && url != "") {
            $.ajax({
                url: url,
                type: type,
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                    if (data != false) {
                        $("#data-container").empty().html(data);
                    }
                }
            });
        }
    });
});

//$(".ajax-request-options").livequery(function () {
//    $(this).click(function (e) {
//        e.preventDefault();
//        //marcar como favorito
//        var id = $(this).attr('id');
//        console.info(id);
//        $("#" + id + ">i.fa-star").addClass("text-warning");
//        $(this).removeClass("ajax-request-options").addClass("ajax-request-unoptions");
//        var type = "GET";
//        if ($(this).hasClass("post")) {
//            type = "POST";
//        }
//        var url = $(this).attr('href');
//        if (url == null || url == "" || url == "#")
//            return false;
//        else {
//            //App.blockUI({ boxed: true });
//            $.ajax({
//                type: type,
//                url: url,
//                cache: false,
//                success: function (data) {
//                    //App.unblockUI();
//                    //App.initAddAlertToPage(data["message"], data["type"]);
//                }
//            });
//        }
//    });
//});

//$(".ajax-request-unoptions").livequery(function () {
//    $(this).click(function (e) {
//        e.preventDefault();
//        var id = $(this).attr('id');
//        console.info(id);
//        $("#" + id + ">i.fa-star").removeClass("text-warning");
//        $(this).removeClass("ajax-request-unoptions").addClass("ajax-request-options");

//        var type = "GET";
//        if ($(this).hasClass("post")) {
//            type = "POST";
//        }
//        var url = $(".ajax-request-unoptions").attr('[data-url]');
//        console.info(url);
//        if (url == null || url == "" || url == "#")
//            return false;
//        else {
//            //App.blockUI({ boxed: true });
//            $.ajax({
//                type: type,
//                url: url,
//                cache: false,
//                success: function (data) {
//                    //App.unblockUI();
//                    //App.initAddAlertToPage(data["message"], data["type"]);
//                }
//            });
//        }
//    });
//});

$('.check-link').livequery(function () {
    $(this).one("click", function (e) {
        e.preventDefault();
        $('.check-link').addClass('check-link-notCompleted');
        $('.check-link-notCompleted').removeClass('check-link');
        var url = $(this).attr("data-done-url");
        var type = "POST";
        if (url != null && url != "") {
            $.ajax({
                url: url,
                type: type,
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                    App.initAddAlertToPage("Tarefa concluída com sucesso!", "success");
                },
                complete: function () {
                    $(this).data('requestRunning', false);
                }
            });
        }
    });
});

$('.check-link-notCompleted').livequery(function () {
    $(this).one("click", function(e){
        e.preventDefault();
        $('.check-link-notCompleted').addClass('check-link');
        $(".check-link").removeClass("check-link-notCompleted");
        
        var url = $(this).attr("data-notDone-url");
        var type = "POST";
        if (url != null && url != "") {
            $.ajax({
                url: url,
                type: type,
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                    App.initAddAlertToPage("Tarefa foi marcada como não concluída!", "info");
                },
                                complete: function () {
                                    $(this).data('requestRunning', false);
                                }
            });
        }
    });
});

//$('.check-link-options').livequery(function () {
//    $(this).one("click", function (e) {
//        e.preventDefault();

//        $(this).addClass("check-link-notOptions");
//        if ($(".check-link-options").hasClass("favorite")) {
//            $(this).find("i").removeClass("fa-star-o");
//            $(this).find("i").addClass("fa-star");
//            $(this).find("i").addClass("text-warning");
//        }
//        if ($(".check-link-options").hasClass("important")) {
//            $(this).find("i").removeClass("fa-bookmark-o");
//            $(this).find("i").addClass("fa-bookmark");
//            $(this).find("i").addClass("text-danger");
//        }
//        $(this).removeClass("check-link-options");
//        var url = $(this).attr("data-url-options");
//        var type = "GET";
//        if (url != null && url != "") {
//            $.ajax({
//                url: url,
//                type: type,
//                async: true,
//                processData: false,
//                cache: false,
//                success: function (data) {
//                    if ($(".check-link-notOptions").hasClass("favorite")) {
//                        App.initAddAlertToPage("Quadro foi marcado como favorito!", "success");
//                    }
//                    if ($(".check-link-notOptions").hasClass("important")) {
//                        App.initAddAlertToPage("Quadro foi marcado como Importante", "success");
//                    }
                    
//                },
//                complete: function () {
//                    $(this).data('requestRunning', false);
//                }
//            });
//        }
//    });
//});

//$('.check-link-notOptions').livequery(function () {
//    $(this).one("click", function (e) {
//        e.preventDefault();
       
//        $(this).addClass("check-link-options");

//        if ($(".check-link-notOptions").hasClass("favorite")) {
//            $(this).find("i").addClass("fa-star-o");
//            $(this).find("i").removeClass("fa-star");
//            $(this).find("i").removeClass("text-warning");
//        }
//        if ($(".check-link-notOptions").hasClass("important")) {
//            $(this).find("i").addClass("fa-bookmark-o");
//            $(this).find("i").removeClass("fa-bookmark");
//            $(this).find("i").removeClass("text-danger");
//        }
//        $(this).removeClass("check-link-notOptions");
//        var url = $(this).attr("data-url-notoptions");
//        var type = "GET";
//        if (url != null && url != "") {
//            $.ajax({
//                url: url,
//                type: type,
//                async: true,
//                processData: false,
//                cache: false,
//                success: function (data) {
//                    if ($(".check-link-notOptions").hasClass("favorite")) {
//                        App.initAddAlertToPage("Quadro foi removido de Favoritos.", "info");
//                    }
//                    if ($(".check-link-notOptions").hasClass("important")) {
//                        App.initAddAlertToPage("Quadro foi removido de Importantes.", "info");
//                    }
//                },
//                complete: function () {
//                    $(this).data('requestRunning', false);
//                }
//            });
//        }
//    });
//});

$('.check-link-favorite').livequery(function () {
    $(this).one("click", function (e) {
        e.preventDefault();
        $(this).find("i").removeClass("fa-star-o");
        $(this).find("i").addClass("fa-star");
        $(this).find("i").addClass("text-warning");

        $(this).removeClass("check-link-favorite");
        $(this).addClass("check-link-notFavorite");
        var url = $(this).attr("data-url-favorite");
        var type = "GET";
        if (url != null && url != "") {
            $.ajax({
                url: url,
                type: type,
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                    App.initAddAlertToPage("Quadro foi marcado como Favorito", "success");

                },
                complete: function () {
                    $(this).data('requestRunning', false);
                }
            });
        }
    });
});

$('.check-link-notFavorite').livequery(function () {
    $(this).one("click", function (e) {
        e.preventDefault();
        $(this).find("i").toggleClass('fa-star').toggleClass('fa-star-o');
        $(this).find("i").removeClass("text-warning");

        $(this).addClass("check-link-favorite");
        $(this).removeClass("check-link-notFavorite");

        var url = $(this).attr("data-url-notfavorite");
        var type = "GET";
        if (url != null && url != "") {
            $.ajax({
                url: url,
                type: type,
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                    App.initAddAlertToPage("Quadro foi removido de Favoritos.", "info");

                },
                complete: function () {
                    $(this).data('requestRunning', false);
                }
            });
        }
    });
});


$('.check-link-important').livequery(function () {
    $(this).one("click", function (e) {
        e.preventDefault();
            $(this).find("i").removeClass("fa-bookmark-o");
            $(this).find("i").addClass("fa-bookmark");
            $(this).find("i").addClass("text-danger");
     
            $(this).removeClass("check-link-important");
            $(this).addClass("check-link-notImportant");
        var url = $(this).attr("data-url-important");
        var type = "GET";
        if (url != null && url != "") {
            $.ajax({
                url: url,
                type: type,
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                        App.initAddAlertToPage("Quadro foi marcado como Importante", "success");

                },
                complete: function () {
                    $(this).data('requestRunning', false);
                }
            });
        }
    });
});

$('.check-link-notImportant').livequery(function () {
    $(this).one("click", function (e) {
        e.preventDefault();
        $(this).find("i").toggleClass('fa-bookmark').toggleClass('fa-bookmark-o');
        $(this).find("i").removeClass("text-danger");

        $(this).addClass("check-link-important");
        $(this).removeClass("check-link-notImportant");

        var url = $(this).attr("data-url-notimportant");
        var type = "GET";
        if (url != null && url != "") {
            $.ajax({
                url: url,
                type: type,
                async: true,
                processData: false,
                cache: false,
                success: function (data) {
                        App.initAddAlertToPage("Quadro foi removido de Importantes.", "info");
                    
                },
                complete: function () {
                    $(this).data('requestRunning', false);
                }
            });
        }
    });
});

//$('.i-checks').livequery(function () {
//    $(this).on('ifChecked', function (event) {
//        var url = $(this).attr("data-done-url");
//        var type = "POST";
//        if (url != null && url != "") {
//            $.ajax({
//                url: url,
//                type: type,
//                async: true,
//                processData: false,
//                cache: false,
//                success: function (data) {
//                    alert("foi");
//                }
//            });
//        }
//    });
//});

//                var url;
//if ($(".ajax-request").hasClass("unfavorite-board")) {
//    url = $(".ajax-request").find("[data-url").val();
//    $("i.fa-star").removeClass("text-warning");
//} else {
//    url = $(this).attr('href');
//}