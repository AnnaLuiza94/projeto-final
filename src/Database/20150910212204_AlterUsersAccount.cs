using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace ProjetoFinal.Database
{
    [Migration(20150910212204)]
    public class Migration20150910212204 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_users", t =>
            {
                t.AddString("state");
                t.AddString("phone_number");
                t.AddDateTime("birth_date");
            });
            
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_users", t =>
            {
                t.RemoveColumn("state");
                t.RemoveColumn("phone_number");
                t.RemoveColumn("birth_date");
            });
        }
    }

}