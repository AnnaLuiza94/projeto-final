using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace ProjetoFinal.Database
{
    [Migration(20150826214906)]
    public class Migration20150826214906 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.AddTable("t_notes", t =>
            {
                t.AddString("name").NotNullable();
                t.AddString("content").NotNullable();
                t.AddDateTime("creation_date").NotNullable();
                t.AddInt32("id_user").NotNullable().AutoForeignKey("t_users");
            });
            
        }

        public override void Down(SchemaAction schema)
        {
            schema.RemoveTable("t_notes");
        }
    }

}