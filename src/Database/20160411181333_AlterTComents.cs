using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace ProjetoFinal.Database
{
    [Migration(20160411181333)]
    public class Migration20160411181333 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_coment", t =>
            {
                t.AddInt32("created_by").AutoForeignKey("t_users");
            });
            
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_coment", t =>
            {
                t.RemoveColumn("created_by");
            });
        }
    }

}