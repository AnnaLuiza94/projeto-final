using Simple.Migrator.Fluent;
using Simple.Migrator.Framework;

namespace ProjetoFinal.Database
{
    [Migration(20160407193443)]
    public class Migration20160407193443 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.RenameTable("t_user_permissions", "t_user_board_permissions");

            schema.ChangeTable("t_user_board_permissions", t =>
            {
                t.RemoveColumn("id_user");
                t.AddInt32("id_user_board").NotNullable().AutoForeignKey("t_users_boards");
            });
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_user_board_permissions", t =>
            {

                t.AddInt32("id_user").NotNullable().AutoForeignKey("t_users");
                t.RemoveColumn("id_user_board");
            });
        }
    }

}