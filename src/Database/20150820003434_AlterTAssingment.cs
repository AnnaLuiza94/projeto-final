using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace ProjetoFinal.Database
{
    [Migration(20150820003434)]
    public class Migration20150820003434 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_assignments", t =>
            {
                t.AddDateTime("edit_date");
                t.AddDateTime("end_date");
            });
            
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_assignments", t =>
            {
                t.RemoveColumn("edit_date");
                t.RemoveColumn("end_date");
            });
        }
    }

}