using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace ProjetoFinal.Database
{
    [Migration(20150818230626)]
    public class Migration20150818230626 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_assignments", t =>
            {
                t.AddDateTime("creation_date");
            });
            
        }

        public override void Down(SchemaAction schema)
        {
            //schema.RemoveTable("books");
        }
    }

}