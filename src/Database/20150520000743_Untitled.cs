using Simple.Migrator.Fluent;
using Simple.Migrator.Framework;

namespace ProjetoFinal.Database
{
    [Migration(20150520000743)]
    public class Migration20150520000743 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.AddTable("t_users", t =>
            {
                t.AddString("name").NotNullable();
                t.AddString("email").NotNullable();
                t.AddBinary("password").NotNullable().WithSize(48);
            });

            schema.AddTable("t_boards", t =>
            {
                t.AddString("name");
                t.AddInt32("id_user").NotNullable().AutoForeignKey("t_users");
                t.AddDateTime("creation_date").NotNullable();
                t.AddBoolean("favorite").Default(false);
                t.AddBoolean("important").Default(false);
                t.AddBoolean("done").Default(false);
            });

            schema.AddTable("t_assignments", t =>
            {
                t.AddBoolean("completed").Default(false);
                t.AddString("content").WithSize(255);
                t.AddInt32("id_board").NotNullable().AutoForeignKey("t_boards");
                t.AddString("name");
            });
            
            
        }

        public override void Down(SchemaAction schema)
        {
            schema.RemoveTable("t_tasks");
            schema.RemoveTable("t_board");
            schema.RemoveTable("t_users");
        }
    }

}