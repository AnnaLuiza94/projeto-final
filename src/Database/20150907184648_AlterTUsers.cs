using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace ProjetoFinal.Database
{
    [Migration(20150907184648)]
    public class Migration20150907184648 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_users", t =>
            {
                t.AddDateTime("creation_date").NotNullable();
                t.AddDateTime("edit_date").NotNullable();
            });
            
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_users", t =>
            {
                t.RemoveColumn("creation_date").NotNullable();
                t.RemoveColumn("edit_date").NotNullable();
            });
        }
    }

}