using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace ProjetoFinal.Database
{
    [Migration(20150822215856)]
    public class Migration20150822215856 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_boards", t =>
            {
                t.AddBoolean("is_archived").Default(false);
            });
            

        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_boards", t =>
            {
                t.RemoveColumn("is_archived");
            });
        }

    }
}