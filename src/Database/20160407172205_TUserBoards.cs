using Simple.Migrator.Fluent;
using Simple.Migrator.Framework;

namespace ProjetoFinal.Database
{
    [Migration(20160407172205)]
    public class Migration20160407172205 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_boards", t =>
            {
                t.RemoveColumn("id_user");
            });

            schema.AddTable("t_users_boards", t =>
            {
                t.AddInt32("id_user").NotNullable().AutoForeignKey("t_users");
                t.AddInt32("id_board").NotNullable().AutoForeignKey("t_boards");
            });
            
        }

        public override void Down(SchemaAction schema)
        {
            schema.RemoveTable("t_users_boards");

            schema.ChangeTable("t_boards", t =>
            {
                t.AddInt32("id_user").NotNullable().AutoForeignKey("t_users");
            });
        }
    }

}