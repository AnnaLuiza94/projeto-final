using Simple.Migrator.Fluent;
using Simple.Migrator.Framework;

namespace ProjetoFinal.Database
{
    [Migration(20151215171317)]
    public class Migration20151215171317 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.AddTable("t_tag", t =>
            {
                t.AddString("name").NotNullable();
                t.AddBoolean("isArchivied");
            });

            schema.ChangeTable("t_boards", t =>
            {
                t.AddInt32("id_tag").AutoForeignKey("t_tag"); 
            });

        
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_boards", t =>
            {
                t.RemoveColumn("id_tag");
            });

            schema.RemoveTable("t_tag");
        }
    }

}