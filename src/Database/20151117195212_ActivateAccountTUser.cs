using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace ProjetoFinal.Database
{
    [Migration(20151117195212)]
    public class Migration20151117195212 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_users", t =>
            {
                t.AddBoolean("activate_account").Default(false).NotNullable();
            });
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_users", t =>
            {
                t.RemoveColumn("activate_account");
            });
        }
    }

}