using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace ProjetoFinal.Database
{
    [Migration(20151209172525)]
    public class Migration20151209172525 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_users", t =>
            {
                t.AddString("image");
            });
            
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_users", t =>
            {
                t.RemoveColumn("image");
            });
        }
    }

}