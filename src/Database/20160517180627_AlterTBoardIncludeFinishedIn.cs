using Simple.Migrator.Fluent;
using Simple.Migrator.Framework;

namespace ProjetoFinal.Database
{
    [Migration(20160517180627)]
    public class Migration20160517180627 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_boards", t =>
            {
                t.AddDateTime("finished_in");
                t.AddDateTime("edit_date");
            });

            schema.ChangeTable("t_assignments", t =>
            {
                t.AddDateTime("finished_in");
            });

            schema.ChangeTable("t_users", t =>
            {
                t.AddString("website");
                t.AddString("token");
            });
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_users", t =>
            {
                t.RemoveColumn("website");
                t.RemoveColumn("token");
            });

            schema.ChangeTable("t_assignments", t =>
            {
                t.RemoveColumn("finished_in");
            });

            schema.ChangeTable("t_boards", t =>
            {
                t.RemoveColumn("finished_in");
            });
        }
    }

}