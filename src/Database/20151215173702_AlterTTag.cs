using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace ProjetoFinal.Database
{
    [Migration(20151215173702)]
    public class Migration20151215173702 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_tag", t =>
            {
                t.AddBoolean("fromSystem").NotNullable();
            });
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_tag", t =>
            {
                t.RemoveColumn("fromSystem");
            });
        }
    }

}