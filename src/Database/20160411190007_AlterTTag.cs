using Simple.Migrator.Fluent;
using Simple.Migrator.Framework;

namespace ProjetoFinal.Database
{
    [Migration(20160411190007)]
    public class Migration20160411190007 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_tag", t =>
            {
                t.AddString("color");
            });


        }

        public override void Down(SchemaAction schema)
        {

            schema.ChangeTable("t_tag", t =>
            {
                t.RemoveColumn("color");
            });
        }
    }

}