using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Simple.Migrator.Framework;
using Simple.Migrator.Fluent;
using System.Data;

namespace ProjetoFinal.Database
{
    [Migration(20150907182423)]
    public class Migration20150907182423 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.AddTable("t_profiles", t =>
            {
                t.AddString("name").NotNullable();   
                t.AddString("description").NotNullable();
            });

            schema.ChangeTable("t_users", t =>
            {
                t.AddInt32("id_profile").NotNullable().AutoForeignKey("t_profiles");
            });
            
        }

        public override void Down(SchemaAction schema)
        {

            schema.ChangeTable("t_user", t =>
            {
                t.RemoveColumn("id_profile");
            });

            schema.RemoveTable("t_profiles");
        }
    }

}