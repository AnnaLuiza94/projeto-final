using Simple.Migrator.Fluent;
using Simple.Migrator.Framework;

namespace ProjetoFinal.Database
{
    [Migration(20160407185215)]
    public class Migration20160407185215 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.AddTable("t_permissions", t =>
            {
                t.AddString("name");
                t.AddInt32("enum_permission");
            });

            schema.AddTable("t_user_permissions", t =>
            {
                t.AddInt32("id_permission").NotNullable().AutoForeignKey("t_permissions");
                t.AddInt32("id_user").NotNullable().AutoForeignKey("t_users");
            });
        }

        public override void Down(SchemaAction schema)
        {
            schema.RemoveTable("t_permissions");
            schema.RemoveTable("t_user_permissions");
        }
    }

}