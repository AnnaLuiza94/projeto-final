using Simple.Migrator.Fluent;
using Simple.Migrator.Framework;

namespace ProjetoFinal.Database
{
    [Migration(20151215172509)]
    public class Migration20151215172509 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.ChangeTable("t_tag", t =>
            {
                t.AddInt32("id_user").NotNullable().AutoForeignKey("t_users");
            });
            
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_tag", t =>
            {
                t.RemoveColumn("id_user");
            });
        }
    }

}