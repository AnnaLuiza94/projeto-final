using Simple.Migrator.Fluent;
using Simple.Migrator.Framework;

namespace ProjetoFinal.Database
{
    [Migration(20160226161139)]
    public class Migration20160226161139 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.AddTable("t_notifications", t =>
            {
                t.AddString("title");
                t.AddString("details");
                t.AddDateTime("creation_date");
                t.AddInt32("id_user").NotNullable().AutoForeignKey("t_users");
            });
            
        }

        public override void Down(SchemaAction schema)
        {
            schema.RemoveTable("t_notifications");
        }
    }

}