using Simple.Migrator.Fluent;
using Simple.Migrator.Framework;

namespace ProjetoFinal.Database
{
    [Migration(20160229164002)]
    public class Migration20160229164002 : FluentMigration
    {
        public override void Up(SchemaAction schema)
        {
            schema.AddTable("t_coment", t =>
            {
                t.AddString("content").NotNullable();
                t.AddDateTime("creation_date").NotNullable();
                t.AddDateTime("edit_date").NotNullable();
                t.AddInt32("id_assignment").NotNullable().AutoForeignKey("t_assignments");
            });

            schema.ChangeTable("t_boards", t =>
            {
                t.AddBoolean("is_shared").Default(false).NotNullable();
            });
        }

        public override void Down(SchemaAction schema)
        {
            schema.ChangeTable("t_boards", t =>
            {
                t.RemoveColumn("is_shared");
            });
            schema.RemoveTable("t_coment");
        }
    }

}